// Ouvre un onglet
const pages = ['niv', 'classes', 'import', 'options', 'rapp', 'elo', 'verifs', 'repartition'];
const options = {
    'langues':[
        'Bilangue ESP',
        'Catalan',
        'Latin',
        'LCE'
    ],
    'sports':[
        'Synchro', 
        'Natation',
        'Triathlon',
        'Pentathlon',
        'Football',
        'Équitation',
        'Volleyball',
        'Tennis',
        'Rugby'
    ],
    'brevets':[
        'BIA',
        'BIMER',
        'JSP'
    ],
    'Autre':[
        'Agro écologie',
        'Chorale'
    ],
    'Dispositifs':[
        'refus SEGPA',
        'ULIS',
        'PAP',
        'PPRE',
        'PAI',
        'PPS'
    ]
}
let config = {
    niveau: '6eme',
    nbClasses: 7,
    choosenOptions: [],
    assosOptions: [],
    selectedMEF:''
}
let editedStudent = 0
let classes = [], groups = [], away = [], MEFS = {}, niveau = {}, oldClasses = {}, filtreClasses = [], filtreOptions = []
let importation = [], listeOptionsDansFichier = []
const champsDeBase = {"N":"Nom","P":"Prénom","C":"Classe","D":"Date naiss","S":"Sexe","cpt":"Comportement","abs":"Absences","niv":"Niveau","M":"MEF Prev","id":"ID","O":"Options","nivFr":"niv Français","nivMath":"niv Maths"};
const champsEleve={"N":"nom","P":"prenom","C":"classe","D":"date","S":"sexe","cpt":"cpt","niv":"niv","abs":"abs","M":"MEF","nivFr":"nivFr","nivMath":"nivMath"};
let correspondances = {};
save = function(what = 'config'){
    if (what === 'config') {
        localStorage.setItem('configv2', JSON.stringify(config));
    } else if (what === 'classes') {
        localStorage.setItem('classesv2', JSON.stringify(classes));
    } else if (what === 'groups') {
        localStorage.setItem('groupsv2', JSON.stringify(groups));
    } else if (what === 'away') {
        localStorage.setItem('awayv2', JSON.stringify(away));
    } else if (what === 'MEFS') {
        localStorage.setItem('MEFSv2', JSON.stringify(MEFS));
    } else if (what === 'niveau') {
        localStorage.setItem('niveauv2', JSON.stringify(niveau));
    } else if (what === 'correspondances') {
        localStorage.setItem('correspondancesv2', JSON.stringify(correspondances));
    }
}
resetDataBase = function(){
    if(confirm('Etes vous sur de vouloir supprimer toutes les données ?\nTout le travail sera à refaire...')){
        // Suppression de configv2, classesv2, groupsv2, awayv2, MEFSv2, niveauv2, correspondancesv2
        localStorage.removeItem('configv2');
        localStorage.removeItem('classesv2');
        localStorage.removeItem('groupsv2');
        localStorage.removeItem('awayv2');
        localStorage.removeItem('MEFSv2');
        localStorage.removeItem('niveauv2');
        localStorage.removeItem('correspondancesv2');
        location.reload();
    }
}
load = function(){
    // Chargement de la config
    const configStore = localStorage.getItem('configv2')
    let restore = false
    if (configStore !== null) {
        config = JSON.parse(configStore);
        restore = true
    }
    // Chargement des classes
    const classesStore = localStorage.getItem('classesv2')
    if (classesStore !== null) {
        classes = JSON.parse(classesStore);
        restore = true
    }
    // Chargement des groupes
    const groupsStore = localStorage.getItem('groupsv2')
    if (groupsStore !== null) {
        groups = JSON.parse(groupsStore);
        restore = true
    }
    // Chargement des away
    const awayStore = localStorage.getItem('awayv2')
    if (awayStore !== null) {
        away = JSON.parse(awayStore);
        restore = true
    }
    // Chargement des MEFS
    const MEFSStore = localStorage.getItem('MEFSv2')
    if (MEFSStore !== null) {
        MEFS = JSON.parse(MEFSStore);
        restore = true
    }
    // Chargement du niveau
    const niveauStore = localStorage.getItem('niveauv2')
    if (niveauStore !== null) {
        niveau = JSON.parse(niveauStore);
        restore = true
    }
    // Chargement des correspondances
    const correspondancesStore = localStorage.getItem('correspondancesv2')
    if (correspondancesStore !== null) {
        correspondances = JSON.parse(correspondancesStore);
        restore = true
    }
    if (restore) {
        restoreInterface();
    }
}
restoreInterface = function(){
    // set niveau
    document.querySelector('#niv input[value="' + config.niveau + '"]').checked = true;
    // set options
    const $options = document.getElementById('optionsList')
    $options.innerHTML = ""
    const $optionsChoisies = document.getElementById('optionsSelected')
    $optionsChoisies.innerHTML = ""
    for (const group in options){
        const optgroup = document.createElement('optgroup')
        optgroup.label = group
        for (const option of options[group]){
            const optionElement = document.createElement('option')
            optionElement.value = option
            optionElement.innerText = option
            if(config.choosenOptions.includes(option)){
                if (document.querySelector('#optionsSelected optgroup[label="' + optgroup.label + '"]') === null){
                    const optgroup = document.createElement('optgroup')
                    optgroup.label = group
                    document.getElementById('optionsSelected').appendChild(optgroup)
                }
                document.querySelector('#optionsSelected optgroup[label="' + optgroup.label + '"]').appendChild(optionElement)
            } else {
                optgroup.appendChild(optionElement)
            }
        }
        $options.appendChild(optgroup)
    }
    // options restantes
    for (const option of config.choosenOptions){
        let found = false
        for (const group in options){
            if(options[group].includes(option)){
                found = true
                break
            }
        }
        if (!found){
            if (document.querySelector('#optionsSelected optgroup[label="Autre"]') === null){
                const optgroup = document.createElement('optgroup')
                optgroup.label = 'Autre'
                document.getElementById('optionsSelected').appendChild(optgroup)
            }
            const optionElement = document.createElement('option')
            optionElement.value = option
            optionElement.innerText = option    
            document.querySelector('#optionsSelected optgroup[label="Autre"]').appendChild(optionElement)
        }
    }
    // set classes
    displayClasses()
    displayVerifs()
    refreshOptions()
}
/**
 * Opens a specific tab by hiding all other tabs.
 *
 * @param {string} tabName - The ID of the tab to be opened.
 * @return {void} This function does not return anything.
 */
function openTab(tabName) {
    for (const tab of pages) {
        document.getElementById(tab).classList.add('is-hidden');
    }
    document.getElementById(tabName).classList.remove('is-hidden');
    moreActionOnTab(tabName)
}
/**
 * Clicks on a tab by triggering a click event on the corresponding tab element.
 *
 * @param {string} tabName - The name of the tab to be clicked.
 * @return {void} This function does not return anything.
 */
function clickOnTab(tabName){
    document.getElementById('tab-' + tabName).click();
    moreActionOnTab(tabName)
}
function moreActionOnTab(tabName){
    if(tabName === 'classes'){
        displayClasses()
    } else if (tabName === 'import'){
        checkIfMEFImported();
    } else if (tabName === 'verifs') {
        displayVerifs()
    } else if (tabName === 'rapp') {
        displayClassesInStep('Rapprochements')
        refreshRapprochements()
    } else if (tabName === 'elo') {
        displayClassesInStep('Eloignements')
        refreshEloignements()
    } else if (tabName === 'repartition') {
        createClassesOfRepartition()
        populateStudentsToPlace()
    }
}
function removeQuotes(string){
    return string.replace(/^"(.+(?="$))"$/, '$1');
}
addOption = function($options){
    $options.querySelectorAll('option').forEach((elt) => {
        if (elt.selected){
            elt.selected = false
            if (document.querySelector('#optionsSelected optgroup[label="' + elt.parentNode.label + '"]') === null){
                const optgroup = document.createElement('optgroup')
                optgroup.label = elt.parentNode.label
                document.getElementById('optionsSelected').appendChild(optgroup)
            }
            document.querySelector('#optionsSelected optgroup[label="' + elt.parentNode.label + '"]').appendChild(elt)
        }
    })
    refreshOptions()
    save()
}
addToOptions = function() {
    const newOption = document.getElementById('newOption')
    if (newOption.value === '') return
    const $option = document.createElement('option')
    $option.value = newOption.value
    $option.innerHTML = newOption.value
    if(document.querySelector('#optionsSelected optgroup[label="Autre"]') === null){
        const optgroup = document.createElement('optgroup')
        optgroup.label = 'Autre'
        document.getElementById('optionsSelected').appendChild(optgroup)
    }
    document.querySelector('#optionsSelected optgroup[label="Autre"]').appendChild($option)
    newOption.value = ''
    refreshOptions()
}
function refreshOptions() {
    config.choosenOptions = []
    filtreOptions = []
    const $options = document.getElementById('optionsSelected').querySelectorAll('option')
    for (const $option of $options){
        config.choosenOptions.push($option.value)
        filtreOptions.push($option.value)
    }
    const $filtresOptions = document.getElementById('filtresOptionsContainer')
    $filtresOptions.innerHTML = ''
    // création des champs de filtres d'options
    const $optionOOptions = document.createElement('option')
    $optionOOptions.value = 'all'
    $optionOOptions.innerText = 'Toutes'
    $filtresOptions.appendChild($optionOOptions)
    for (const i of filtreOptions) {
        const $option = document.createElement('option')
        $option.value = i
        $option.innerText = i
        $filtresOptions.appendChild($option)
    }
}

function parseFile(result){
    const lignes = result.split(/[\r\n]+/g);
    importation = lignes;
    // on affiche la première ligne, avec une case pour la cocher si ce sont les noms de champs
    // on affiche la 2e ligne (non vide)
    // on afffiche dessous les choix de champs possibles
    const assocs = document.getElementById("leschamps");
    assocs.innerHTML = "";
    const premiere = lignes[0].split(/[\t;]/g).map(removeQuotes);
    const deuxieme = lignes[1].split(/[\t;]/g).map(removeQuotes);
    const div0 = document.createElement("DIV");
    div0.innerText = "Première ligne du fichier";
    assocs.appendChild(div0);
    const table = document.createElement("TABLE");
    const tr1 = document.createElement("TR");
    tr1.className = 'has-text-primary';
    const tr2 = document.createElement("TR");
    const tr3 = document.createElement("TR");
    for (let i = 0, len = premiere.length; i < len; i++) {
        const td = document.createElement("TD");
        td.innerText = premiere[i];
        tr1.appendChild(td);
        const td2 = document.createElement("TD");
        td2.innerText = deuxieme[i];
        tr2.appendChild(td2);
        const td3 = document.createElement("TD");
        tr3.appendChild(td3)
        const select = document.createElement("SELECT");
        const option=document.createElement("OPTION");
        option.value=0;
        option.disabled=true;
        option.selected = true;
        option.innerText = "Sélectionner";
        select.appendChild(option);
        for(let j in champsDeBase) {
            const option = document.createElement("OPTION");
            option.value=j;
            option.innerText=champsDeBase[j];
            select.appendChild(option);
            if (correspondances[j] === i){
                option.selected = true;
                select.style.backgroundColor = "greenyellow";
            } else if(champsDeBase[j]==premiere[i]){
                select.style.backgroundColor = "greenyellow";
                option.selected = true;
                correspondances[j]=i;
            }
        }
        select.id = i; // numero du champ
        select.onchange = function(){
            let corresExist = false;
            if(correspondances["O"] !== undefined && this.value === "O") {
                corresExist = true;
            }
            if(select.selectedIndex>0){
                select.style.backgroundColor = "greenyellow";
            } else {
                select.style.backgroundColor = "";
            }
            if(this.value === "O"){
                for(let k=1,len=importation.length;k<len;k++) {
                    if(importation[k]==="")continue;
                    const values = importation[k].split(/[\t;]/).map(removeQuotes);
                    const options = values[this.id].split(",");
                    if(corresExist){
                        // on ajoute les options dans le 1er champ désigné
                        values[correspondances["O"]] = values[correspondances["O"]]+","+values[this.id];
                        importation[k]=values.join(",");
                    }
                    for(let m=0,len=options.length;m<len;m++){
                        if(listeOptionsDansFichier.indexOf(options[m].trim())<0 && options[m].trim()!==""){
                            listeOptionsDansFichier.push(options[m].trim());
                        }
                    }
                }
                // affichage des associations d'options
                const div=document.getElementById("lesoptions"); 
                div.innerHTML = "";
                for(let n=0,len=listeOptionsDansFichier.length;n<len;n++){
                    const sousdiv = document.createElement("DIV");
                    sousdiv.innerHTML = listeOptionsDansFichier[n];
                    const select2 = document.createElement("SELECT");
                    select2.dataset.nom=listeOptionsDansFichier[n];
                    const option = document.createElement("OPTION");
                    option.innerText = "Selectionner";
                    option.value=0;
                    select2.appendChild(option);
                    for(let m=0,len=config.choosenOptions.length;m<len;m++){
                        const option = document.createElement("OPTION");
                        option.innerText = config.choosenOptions[m];
                        option.value=m+1;
                        select2.appendChild(option);
                        for (const assoc in config.assosOptions){
                            if(config.assosOptions[assoc][1]===listeOptionsDansFichier[n]){
                                option.selected=true;
                                break;
                            }
                        }
                    }
                    select2.onchange = () => {
                        if (Number(select2.value) > 0)
                            config.assosOptions.push([select2.selectedOptions[0].innerText, select2.dataset.nom])
                        else config.assosOptions = config.assosOptions.filter(assoc => assoc[1] !== select2.dataset.nom && assoc[0] !== select2.selectedOptions[0].innerText)
                    }
                    sousdiv.appendChild(select2);
                    div.appendChild(sousdiv);
                }
            }
            // on ajoute la liste des options et la possibilité de l'associer avec les codes donnés.
            if(!corresExist)
                correspondances[this.value] = Number(this.id);
        }
        td3.appendChild(select)
    }
    table.appendChild(tr1);
    table.appendChild(tr2);
    table.appendChild(tr3);
    assocs.appendChild(table);
}
function importPhase2(){
    save()
    save('correspondances')
    if(correspondances["M"]=== undefined){
        alert("Veuillez indiquer le champ du MEF prévisionnel !");
        return;
    }
    // vérification qu'il n'y a pas 2 champs qui donnent la même chose
    const trace=[];
    for(const i in correspondances){
        if(trace.indexOf(correspondances[i])>-1){
            alert("Deux champs ont la même correspondance !\nVeuillez corriger.");
            return;
        }
        trace.push(correspondances[i]);
    }
    const lignes = importation;
    if(document.getElementById("firstLine").checked){
        lignes.shift();
    }
    const nbl = lignes.length;
    for(var i=0; i<nbl; i++){
        let index;
        const champs = lignes[i].split(/[\t;]/).map(this.removeQuotes);
        if(correspondances["id"] === undefined){// le premier champ est un texte
            index = i;
        } else index = champs[correspondances["id"]];
        if(champs[correspondances["classe"]]=="" || champs.length<2)continue; // si pas de classe on saute (il a quitté l'étab)
        const el = {
            verrou:false, // mettre à true si bloqué dans sa classe
            newclasse:'none'
        };
        for(let j in correspondances){
            if(j!=="O") el[champsEleve[j]] = champs[correspondances[j]];
        }
        // traitement des options
        if(correspondances["O"] !== undefined){
            for(const opt in config.assosOptions){
                if(champs[correspondances["O"]].indexOf(config.assosOptions[opt][1])>-1){
                    el[config.assosOptions[opt][0]] = true;
                } else {
                    el[config.assosOptions[opt][0]] = false;
                }
            }
        }
        // ajout des options sélectionnées dans la 2e phase de configuration
        for (const opt of config.choosenOptions){
            if(el[opt] === undefined) el[opt] = false
        }
        if(MEFS[champs[correspondances["M"]].substring(0,4)] === undefined){// MEFS à option supprimés
            MEFS[champs[correspondances["M"]].substring(0,4)] = {};
        }
        MEFS[champs[correspondances["M"]].substring(0,4)][index] = el;
    }
    save('MEFS')
    alert("Importation effectuée avec succès de " + nbl + " élèves.")
    checkIfMEFImported()
}
function getDataFromTextArea(){
    if (!document.getElementById("textareaImport").value) return
    parseFile(document.getElementById("textareaImport").value)
}
function uploadFile(evt){
    const input = evt.target
    const reader = new FileReader();
    reader.onload = function(){
        parseFile(reader.result);
    }
    reader.readAsText(input.files[0]);
}
function openFile(){
    const reader = new XMLHttpRequest();
    reader.onload = function(){
        parseFile(reader.responseText);
    }
    reader.onerror = err=>{reject(err)};;
    reader.open("get", "Eleves 2024.csv");
    reader.send();
}
function displayClasses(){
    const $classes = document.getElementById('classListToPrepare')
    if(config.nbClasses > $classes.children.length){
        for(let i = $classes.children.length; i < config.nbClasses; i++){
            if(classes[i] === undefined){
                classes.push({name: config.niveau + String(i + 1), students: [], options: []})
            }
            const $classe = document.createElement('div')
            $classe.id = 'classe' + i
            $classe.classList.add('class-options')
            const $label = document.createElement('label')
            $label.htmlFor = 'classe' + i
            $label.innerText = config.niveau +' ' + (i + 1)
            $classe.appendChild($label)
            $classe.appendChild(document.createElement('br'))
            for (const [index, option] of config.choosenOptions.entries()) {
                const $input = document.createElement('input')
                $input.type = 'checkbox'
                $input.name = 'classe' + i+'_'+index
                $input.id = 'classe' + i+'_'+index
                $input.className = 'is-checkradio is-block is-info'
                $input.value = option
                if (classes[i].options.includes(option)) {
                    $input.checked = true
                }
                $classe.appendChild($input)
                const $label = document.createElement('label')
                $label.htmlFor = 'classe' + i+'_'+index
                $label.innerText = option
                $classe.appendChild($label)
                $classe.appendChild(document.createElement('br'))
                $input.onclick = (evt) => {
                    if(evt.target.checked){
                        classes[i].options.push(evt.target.value)
                    } else {
                        classes[i].options = classes[i].options.filter((elt) => elt !== evt.target.value)
                    }
                    save('classes')
                }
            }
            $classes.appendChild($classe)
        }
    } else if(config.nbClasses < $classes.children.length){
        for(let i = $classes.children.length - 1; i >= config.nbClasses; i--){
            classes.pop()
            $classes.removeChild($classes.children[i])
        }
    }
}
function toDateHTML(date){
    if(date.indexOf("/")<0) return "?";
    let elts = date.split("/");
    if(elts[2].length === 2) elts[2] = '20'+elts[2];
    return elts[2]+"-"+elts[1]+"-"+elts[0];
}
function toDateFr(date){
    if(date.indexOf("-")<0) return "?";
    let elts = date.split("-");
    return elts[2]+"/"+elts[1]+"/"+elts[0];
}
function displayVerifs(){
    const $verif = document.getElementById('verif')
    $verif.innerHTML = ''
    oldClasses = {}, filtreClasses = []
    // tri des élèves par classe d'origine
    for (const eleveId in niveau) {
        const eleve = niveau[eleveId]
        if (oldClasses[eleve.classe] === undefined) {
            oldClasses[eleve.classe] = []
            filtreClasses.push(eleve.classe)
        }
        /** DANGER : lors de suppression d'un élève, les id changent ? */
        eleve.id = Number(eleveId)
        oldClasses[eleve.classe].push(Number(eleveId))
    }
    // creation des filtres de classes dans la page répartition
    const $filtresClasses = document.getElementById('filtresClassesContainer')
    $filtresClasses.innerHTML = ''
    const $option0 = document.createElement('option')
    $option0.value = 'all'
    $option0.innerText = 'Toutes'
    $filtresClasses.appendChild($option0)
    filtreClasses.sort((a, b) => (a > b) ? 1 : -1)
    // création des champs de filtres de classes
    for (const i of filtreClasses) {
        const $option = document.createElement('option')
        $option.value = i
        $option.innerText = i
        $filtresClasses.appendChild($option)
    }

    // tri par ordre alpha des élèves dans les classes
    for (const classe in oldClasses) {
        oldClasses[classe].sort((a, b) => (niveau[a].nom > niveau[b].nom) ? 1 : -1)
        const $classe = document.createElement('div')
        $classe.classList.add('div-verif-classe')
        const title = document.createElement('h3')
        title.className = 'title'
        title.innerText = 'ex ' + classe
        $classe.appendChild(title)
        for (const eleveIndex in oldClasses[classe]) {
            const eleve = niveau[oldClasses[classe][eleveIndex]]
            const $eleve = document.createElement('div')
            $eleve.classList.add('verif-eleve')
            // sexe
            if(eleve.sexe === 'G'){
                $eleve.classList.add('boy')
            } else {
                $eleve.classList.add('girl')
            }
            const $nom = document.createElement('div')
            $nom.innerText = eleve.nom + ' ' + eleve.prenom
            $nom.className = 'eleve-nom'
            $eleve.appendChild($nom)
            // niveaux
            const $niveaux = document.createElement('div')
            $niveaux.className = 'eleve-niveaux'
            const $niv = document.createElement('div')
            $niv.className = eleve.niv
            $niv.innerHTML = ' '
            $niveaux.appendChild($niv)
            // abs
            const $abs = document.createElement('div')
            $abs.className = eleve.abs
            $abs.innerHTML = ' '
            $niveaux.appendChild($abs)
            // cpt
            const $cpt = document.createElement('div')
            $cpt.className = eleve.cpt
            $cpt.innerHTML = ' '
            $niveaux.appendChild($cpt)
            $eleve.appendChild($niveaux)
            $classe.appendChild($eleve)
            $eleve.onclick = () => {
                const $studentEditor = document.getElementById('studentEditor')
                $studentEditor.classList.add('is-active')
                studentEdit(eleve.id)
            }
        }    
        $verif.appendChild($classe)
    }
}
function displayClassesInStep(where){
    if (where === undefined) {
        return
    }
    const $classes = document.getElementById('classes'+where)
    $classes.innerHTML = ''
    for(const classe in oldClasses) {
        const $classe = document.createElement('div')
        $classe.className = 'classe'
        $classe.innerText = 'ex ' + classe
        $classes.appendChild($classe)
        for (const eleveIndex in oldClasses[classe]) {
            const eleve = niveau[oldClasses[classe][eleveIndex]]
            const $eleve = document.createElement('div')
            $eleve.className = 'eleve'
            const $input = document.createElement('input')
            $input.type = 'checkbox'
            $input.value = eleve.id
            $eleve.appendChild($input)
            const $span = document.createElement('span')
            $span.innerText += eleve.nom + ' ' + eleve.prenom
            $eleve.appendChild($span)
            if (eleve.sexe === 'G') {
                $eleve.classList.add('boy')
            } else {
                $eleve.classList.add('girl')
            }
            // groupé ?
            if (where === 'Rapprochements') {
                checkIfLinked(eleve.id, $eleve)
            } else if (where === 'Eloignements') {
                checkIfAway(eleve.id, $eleve)
            }
            $classe.appendChild($eleve)
            $eleve.onclick = () => {
                $input.checked = !$input.checked
            }
            $classe.appendChild($eleve)
        }
    }
}
function checkIfLinked(studentId, $element){
    for (const group of groups) {
        if (group.includes(Number(studentId))) {
            $element.classList.add('linked')
            break
        }
    }
}
function checkIfAway(studentId, $element){
    for (const awayGroup of away) {
        if (awayGroup.includes(Number(studentId))) {
            $element.classList.add('away')
            break
        }
    }
}
function addRapprochements() {
    const aRapprocher = document.querySelectorAll('#classesRapprochements input[type="checkbox"]:checked, #repartition input[type="checkbox"]:checked')
    if (aRapprocher.length < 2) return
    const gp = []
    for (const eleve of aRapprocher) {
        gp.push(Number(eleve.value))
        eleve.parentNode.classList.add('linked')
        eleve.checked = false
    }
    groups.push(gp)
    save('groups')
    refreshRapprochements()
}
function addEloignements() {
    const aEloigner = document.querySelectorAll('#classesEloignements input[type="checkbox"]:checked, #repartition input[type="checkbox"]:checked')
    if (aEloigner.length < 2) return
    const gp = []
    for (const eleve of aEloigner) {
        gp.push(Number(eleve.value))
        eleve.parentNode.classList.add('linked')
        eleve.checked = false
    }
    away.push(gp)
    save('away')
    refreshEloignements()
}
function refreshRapprochements(){
    const $rapp = document.getElementById('rapprochements')
    $rapp.innerHTML = ''
    for (const group of groups) {
        const $group = document.createElement('div')
        $group.className = 'groupRapp panel'
        let groupExists = true
        for (const eleve of group) {
            const $eleve = document.createElement('div')
            $eleve.className = 'eleve'
            const elev = niveau[eleve]
            if (elev === undefined){
                // delete group from groups
                groups.splice(groups.indexOf(group), 1)
                save('groups')
                groupExists = false
                break
            }
            $eleve.innerText = elev.classe + ' - ' + elev.nom + ' ' + elev.prenom
            $group.appendChild($eleve)
        }
        if (!groupExists) continue
        const $button = document.createElement('button')
        $button.className = 'delete'
        $button.innerText = ' '
        $button.onclick = () => {
            groups.splice(groups.indexOf(group), 1)
            save('groups')
            refreshRapprochements()
        }
        $group.prepend($button)
        $rapp.appendChild($group)
    }
}
function refreshEloignements(){
    const $elo = document.getElementById('eloignements')
    $elo.innerHTML = ''
    for (const gp of away) {
        const $group = document.createElement('div')
        $group.className = 'groupElo panel'
        let groupExists = true
        for (const eleve of gp) {
            const $eleve = document.createElement('div')
            $eleve.className = 'eleve'
            const elev = niveau[eleve]
            if (elev === undefined) {
                console.log('elev undefined', eleve)
                // delete group from away
                away.splice(away.indexOf(gp), 1)
                save('away')
                groupExists = false
                break
            }
            $eleve.innerText = elev.classe + ' - ' + elev.nom + ' ' + elev.prenom
            $group.appendChild($eleve)
        }
        if (!groupExists) continue
        const $button = document.createElement('button')
        $button.className = 'delete'
        $button.innerText = ' '
        $button.onclick = () => {
            away.splice(away.indexOf(gp), 1)
            save('away')
            refreshEloignements()
        }
        $group.prepend($button)
        $elo.appendChild($group)
    }
}
function addStudent() {
    // get last id
    let id = 0
    for (const eleveId in niveau) {
        if (eleveId > id) id = eleveId
    }
    // copie du dernier élève pour modèle
    const studentModel = niveau[id]
    const newStudent = { ...studentModel }
    newStudent.nom = ''
    newStudent.prenom = ''
    newStudent.sexe = 'G'
    newStudent.date = ''
    newStudent.cpt = 'B'
    newStudent.abs = 'B'
    newStudent.niv = 'B'
    newStudent.nivFr = 'B'
    newStudent.nivMath = 'B'
    newStudent.classe = 'ext'
    newStudent.newclasse = 0
    for (const el in newStudent) {
        if (typeof newStudent[el] === 'boolean') newStudent[el] = false
    }
    id++
    newStudent.id = id
    niveau[id] = newStudent
    save('niveau')
    document.getElementById('studentEditor').classList.add('is-active')
    studentEdit(id, true)
}
function studentEdit(eleveId, newStudent = false) {
    document.getElementById('studentIdSpan').innerText = eleveId
    $editor = document.getElementById('studentEditor')
    editedStudent = eleveId
    const eleve = niveau[eleveId]
    const $name = $editor.querySelector('#studentNom')
    $name.value = eleve.nom
    $name.onchange = () => {
        niveau[eleveId].nom = $name.value
    }
    const $surname = $editor.querySelector('#studentPrenom')
    $surname.value = eleve.prenom
    $surname.onchange = () => {
        niveau[eleveId].prenom = $surname.value
    }
    $editor.querySelector('#studentSexe'+eleve.sexe).checked = true
    $editor.querySelector('#studentSexeF').onclick = () => {
        niveau[eleveId].sexe = 'F'
    }
    $editor.querySelector('#studentSexeG').onclick = () => {
        niveau[eleveId].sexe = 'G'
    }
    const birth = $editor.querySelector('#studentDate')
    birth.value = toDateHTML(eleve.date)
    birth.onchange = () => {
        niveau[eleveId].date = toDateFr(birth.value)
    }
    const $classe = $editor.querySelector('#studentClasseSelect') // select
    $classe.innerHTML = ''
    if (newStudent && oldClasses['ext'] === undefined) {
        const option = document.createElement('option')
        option.value = 'ext'
        option.innerText = 'ext'
        $classe.appendChild(option)
        option.selected = true
    }
    for (const i in oldClasses) {
        const option = document.createElement('option')
        option.value = i
        option.innerText = i
        $classe.appendChild(option)
        if (i === eleve.classe) {
            option.selected = true
        }
    }
    $classe.onchange = () => {
        niveau[eleveId].classe = $classe.value
        // on recalcule les classes
        displayVerifs()
    }
    if (eleve.niv === '') eleve.niv = 'B'
    if (eleve.cpt === '') eleve.cpt = 'B'
    if (eleve.abs === '') eleve.abs = 'B'
    if (eleve.nivFr === '' || eleve.nivFr === undefined) eleve.nivFr = 'B'
    if (eleve.nivMath === '' || eleve.nivMath === undefined) eleve.nivMath = 'B'
    $editor.querySelector('#niv'+eleve.niv).checked = true
    $editor.querySelector('#abs'+eleve.abs).checked = true
    $editor.querySelector('#cpt'+eleve.cpt).checked = true;
    $editor.querySelector('#nivFr'+eleve.nivFr).checked = true;
    $editor.querySelector('#nivMath'+eleve.nivMath).checked = true;
    ['nivA', 'nivB', 'nivC', 'nivD', 'absA', 'absB', 'absC', 'cptA', 'cptB', 'cptC', 'nivFrA', 'nivFrB', 'nivFrC', 'nivFrD', 'nivMathA', 'nivMathB', 'nivMathC', 'nivMathD'].forEach(id => {
        $editor.querySelector('#'+id).onclick = () => {
            niveau[eleveId][id.slice(0, -1)] = id.slice(-1)
        }
    });
    $choixStudentOptions = document.getElementById('choixStudentOptions')
    $choixStudentOptions.innerHTML = ''
    for (const option of config.choosenOptions) {
        const $div = document.createElement('div')
        const $input = document.createElement('input')
        $input.type = 'checkbox'
        $input.name = option + '_' + eleveId
        $input.id = option + '_' + eleveId
        $input.className = 'is-checkradio is-block is-info'
        $input.value = option
        if (eleve[option]) {
            $input.checked = true
        }
        $div.appendChild($input)
        const $label = document.createElement('label')
        $label.htmlFor = option + '_' + eleveId
        $label.innerText = option
        $div.appendChild($label)
        $choixStudentOptions.appendChild($div)
        $input.onclick = (evt) => {
            if (evt.target.checked) {
                niveau[eleveId][option] = true
            } else {
                niveau[eleveId][option] = false
            }
        }
    }
}
function checkIfMEFImported() {
    let isImported = false
    for (const i in MEFS) {
        for (const j in MEFS[i]) {
            isImported = true
            break
        }
    }
    if (isImported) {
        const $info = document.querySelector('#fileToImport span.file-label')
        $info.innerHTML = 'Niveau déjà importé'
        $info.classList.add('has-text-danger')
        const select = document.getElementById('select-MEF')
        select.className = 'select'
        select.innerHTML = ''
        const option = document.createElement('option')
        option.value = 0
        option.innerText = 'Choix du MEF'
        select.appendChild(option)
        for (const i in MEFS) {
            const option = document.createElement('option')
            option.value = i
            option.innerText = i
            select.appendChild(option)
            if(i === config.selectedMEF){
                option.selected = true
            }
        }
        select.onchange = () => {
            config.selectedMEF = select.value
            niveau = MEFS[select.value]
            save('niveau')
            alert('MEF travaillé : '+ select.value)
        }
    }
}
function moveAllLinkedStudentsTo(studentId, destination) {
    let moved = false
    for (const links of groups) {
        if (links.includes(Number(studentId))) {
            for (const id of links) {
                if (id !== Number(studentId)) {
                    if (destination !== 'none') {
                        // check compatibility of options
                        const [compatible, incompatible] = checkCompatibilityOfOptions(id, destination)
                        if (!compatible) {
                            alert('Incompatibilité d’option pour ' + niveau[id].prenom + ' ' + niveau[id].nom + ' : ' + incompatible.join(', '))
                            break
                        }
                    }
                    if (niveau[id].newclasse !== 'none') {
                        classes[Number(niveau[id].newclasse)].students.splice(classes[Number(niveau[id].newclasse)].students.indexOf(id), 1)
                    }
                    if (destination !== 'none') {
                        classes[destination].students.push(id)
                    }
                    niveau[id].newclasse = destination
                    moved = true
                }
            }
        }
    }
    if (moved) save('niveau')
}
function checkIfAwayInClass(studentId, destination) {
    if (destination === 'none') return true
    const listOfStudents = []
    for (const list of away) {
        if (list.includes(studentId)) {
            for (const id of list) {
                if (id !== studentId) {
                    if (niveau[id].newclasse === destination) {
                        listOfStudents.push(niveau[id].nom + ' ' + niveau[id].prenom)
                    }
                }
            }
        }
    }
    if (listOfStudents.length > 0){
        if (confirm('Cet élève est à éloigner de ' + listOfStudents.join(', ') + ' déjà présent(s) dans la classe. Voulez-vous continuer ?')) {
            return true
        } else return false
    } else return true
}
function placeAllStudentsToStep1() {
    document.getElementById('repartitionContainer').classList.toggle('show-buttons')
}
function placeAllStudentsToStep2(destination) {
    // check incompatibilities
    let compatible = true
    const listOfStudents = document.querySelectorAll('#disponiblesContainer .verif-eleve')
    const arrayOfStudents = Array.from(listOfStudents)
    arrayOfStudents.every((eleve) => {
        if (!checkIfAwayInClass(Number(eleve.dataset.id), destination)) {
            compatible = false
            return false
        }
        let [compatible, incompatibleOptions] = checkCompatibilityOfOptions(Number(eleve.dataset.id), destination)
        if (compatible === false) {
            alert('Incompatibilité d’option pour ' + niveau[Number(eleve.dataset.id)].prenom + ' ' + niveau[Number(eleve.dataset.id)].nom + ' : ' + incompatibleOptions.join(', '))
            compatible = false
            return false
        }
        return true
    })
    if (!compatible) return
    document.querySelectorAll('#disponiblesContainer .verif-eleve').forEach((eleve) => {
        classes[destination].students.push(Number(eleve.dataset.id))
        niveau[Number(eleve.dataset.id)].newclasse = Number(destination)
    })
    save('niveau')
    save('classes')
    createClassesOfRepartition()
    populateStudentsToPlace()
    document.getElementById('repartitionContainer').classList.toggle('show-buttons')
}
function checkCompatibilityOfOptions(studentId, classeId) {
    let compatible = true
    let incompatibleOptions = []    
    for (const option of config.choosenOptions) {
        if (!niveau[studentId][option]) continue
        if (classes[classeId].options.indexOf(option) < 0) {
            incompatibleOptions.push(option)
            compatible = false
        }
    }
    return [compatible, incompatibleOptions]
}
function emptyClass(classeId) {
    if (confirm('Voulez-vous vider la classe ' + classes[classeId].name + ' ?')) {
        classes[classeId].students = []
        save('classes')
        createClassesOfRepartition()
        populateStudentsToPlace()
    }
}
function reveal(text){
    if (text.length < 3) return
    // recherche du nom dans les éléments affichés
    document.querySelectorAll('#repartition .eleve-nom').forEach(eleve => {
        if (eleve.innerText.toLowerCase().includes(text.toLowerCase())) {
            eleve.parentNode.classList.add('horizontal-shake')
            setTimeout(() => eleve.parentNode.classList.remove('horizontal-shake'), 2000)
        }
    })
}
function selectAll(classeId) {
    document.querySelectorAll('#classe'+classeId +' .verif-eleve input[type="checkbox"]').forEach(eleve => {
        eleve.checked = true
    })
}
function createClassesOfRepartition() {
    if (classes.length === 0) return
    const selectedOption = document.getElementById('filtresOptionsContainer').value
    const selectedClasse = document.getElementById('filtresClassesContainer').value
    const selectedNiveau = document.getElementById('filtreNiveaux').value
    const selectedComportement = document.getElementById('filtreComportement').value
    const selectedAbsence = document.getElementById('filtreAbsences').value
    const selectedNivFr = document.getElementById('filtreFrancais').value
    const selectedNivMath = document.getElementById('filtreMaths').value
    const $repartitionContainer = document.getElementById('repartitionContainer')
    $repartitionContainer.innerHTML = ''
    for (const i in classes) {
        const $div = document.createElement('div')
        $div.id = 'classe' + i
        $div.dataset.id = i
        $div.className = 'is-flex is-flex-direction-column is-align-items-center classe'
        const $label = document.createElement('label')
        $label.innerHTML = classes[i].name
        const btn = document.createElement('button')
        btn.className = 'button is-hidden is-size-7'
        btn.innerText = 'Ici'
        btn.onclick = () => {
            placeAllStudentsToStep2(i)
        }
        const btnEmpty = document.createElement('button')
        btnEmpty.className = 'delete'
        btnEmpty.onclick = () => {
            emptyClass(i)
        }
        const btnSelectAll = document.createElement('button')
        btnSelectAll.className = 'button is-hidden select-all is-size-7'
        btnSelectAll.innerText = '✔'
        btnSelectAll.onclick = () => {
            selectAll(i)
        }
        $label.appendChild(btnEmpty)
        $label.appendChild(btnSelectAll)
        $label.appendChild(btn)
        $div.appendChild($label)
        $divContainer = document.createElement('div')
        $divContainer.className = 'is-flex is-flex-direction-column is-align-items-center m-3'
        $div.appendChild($divContainer)
        $repartitionContainer.appendChild($div)
		interact($div).dropzone({
			ondrop: function (event) {
                const classeId = Number(event.target.dataset.id)
                const studentId = Number(event.relatedTarget.dataset.id)
                if (!checkIfAwayInClass(studentId, classeId)) {
                    return
                }
                if (!checkCompatibilityOfOptions(studentId, classeId)[0]){
                    if (!confirm('Les options '+checkCompatibilityOfOptions(studentId, classeId)[1].join(', ')+' proposées ne sont pas compatibles avec celle de la classe : ' + classes[classeId].options.join(', ') + '. Voulez-vous continuer ?')) {
                        return
                    }
                }
                // if from other class
                if (event.relatedTarget.dataset.classe !== undefined) {
                    let oldClasse = Number(event.relatedTarget.dataset.classe)
                    // remove from other class
                    classes[oldClasse].students.splice(classes[oldClasse].students.indexOf(studentId), 1)
                }
                classes[classeId].students.push(studentId)
                niveau[studentId].newclasse = classeId
                moveAllLinkedStudentsTo(studentId, classeId)
                classes[classeId].students.sort((a, b) => (niveau[a].nom > niveau[b].nom) ? 1 : -1)
				save('classes')
				createClassesOfRepartition()
				populateStudentsToPlace()
			}
		  })
		  .on('dropactivate', function (event) {
			event.target.classList.add('drop-activated')
		  })
        // create students
        const stats = {
            total : 0,
            sexe : {
                G : 0, 
                F : 0
            },
            niv : {
                A : 0,
                B : 0,
                C : 0,
                D : 0
            },
            abs : {
                A : 0,  
                B : 0,
                C : 0
            },
            cpt : {
                A : 0,
                B : 0,
                C : 0
            },
            nivFr : {
                A : 0,
                B : 0,
                C : 0,
                D : 0
            },
            nivMath : {
                A : 0,
                B : 0,
                C : 0,
                D : 0
            },
            classesProv : {

            },
            annees: {
            }
        }
        for (const el of classes[i].students) {
            if (isNaN(el) || el === null) {
                // delete student from classes
                classes[i].students.splice(classes[i].students.indexOf(el), 1)
                continue
            }
			const eleve = niveau[el]
            const $eleve = document.createElement('div')
            if (!eleve.verrou)
                $eleve.className = 'verif-eleve draggable'
            else 
                $eleve.className = 'verif-eleve locked'
            $eleve.dataset.id = el
            $eleve.dataset.classe = i
            const $input = document.createElement('input')
            $input.type = 'checkbox'
            $input.value = el
            $eleve.appendChild($input)
            stats.total++
            // sexe
            if(eleve.sexe === 'G'){
                $eleve.classList.add('boy')
                stats.sexe.G++
            } else {
                $eleve.classList.add('girl')
                stats.sexe.F++
            }
            if(selectedOption !== 'all') {
                if (!eleve[selectedOption]) {
                    $eleve.classList.add('grayscale')
                }
            }
            if (selectedClasse !== 'all') {
                if (eleve.classe !== selectedClasse) {
                    $eleve.classList.add('grayscale')
                }
            }
            if (selectedAbsence !== 'all') {
                if (eleve.abs !== selectedAbsence) {
                    $eleve.classList.add('grayscale')
                }
            }
            if (selectedNiveau !== 'all') {
                if (eleve.niv !== selectedNiveau) {
                    $eleve.classList.add('grayscale')
                }
            }
            if (selectedComportement !== 'all') {
                if (eleve.cpt !== selectedComportement) {
                    $eleve.classList.add('grayscale')
                }
            }
            if (selectedNivFr !== 'all') {
                if (eleve.nivFr !== selectedNivFr) {
                    $eleve.classList.add('grayscale')
                }
            }
            if (selectedNivMath !== 'all') {
                if (eleve.nivMath !== selectedNivMath) {
                    $eleve.classList.add('grayscale')
                }
            }
            checkIfLinked(el, $eleve)
            checkIfAway(el, $eleve)
            const $nom = document.createElement('div')
            $nom.innerText = eleve.nom + ' ' + eleve.prenom
            $nom.className = 'eleve-nom'
            $eleve.appendChild($nom)
            const $classe = document.createElement('div')
            $classe.innerText = eleve.classe
            $classe.className = 'eleve-classe'
            $eleve.appendChild($classe)
    
            // niveaux
            const $niveaux = document.createElement('div')
            $niveaux.className = 'eleve-niveaux'
            const $niv = document.createElement('div')
            $niv.className = eleve.niv
            $niv.innerHTML = ' '
            $niveaux.appendChild($niv)
            stats.niv[eleve.niv]++
            // abs
            const $abs = document.createElement('div')
            $abs.className = eleve.abs
            $abs.innerHTML = ' '
            $niveaux.appendChild($abs)
            stats.abs[eleve.abs]++
            // cpt
            const $cpt = document.createElement('div')
            $cpt.className = eleve.cpt
            $cpt.innerHTML = ' '
            $niveaux.appendChild($cpt)
            stats.cpt[eleve.cpt]++
            // classesProv
            stats.classesProv[eleve.classe] = stats.classesProv[eleve.classe] || 0
            stats.classesProv[eleve.classe]++
            // annees
            const annee = eleve.date.split('/')[2]
            stats.annees[annee] = stats.annees[annee] || 0
            stats.annees[annee]++
            // total
            $eleve.appendChild($niveaux)
            $divContainer.appendChild($eleve)
        }
        // stats
        const $stats = document.createElement('div')
        $stats.className = 'classe-stats'
        $stats.innerHTML = 'Total : ' + stats.total
        $div.appendChild($stats)
        const $stats2 = document.createElement('table')
        $stats2.className = 'eleve-stats'
        const tr1 = document.createElement('tr')
        const td11 = document.createElement('td')
        td11.innerText = 'G/F'
        tr1.appendChild(td11)
        const td12 = document.createElement('td')
        tr1.appendChild(td12)
        if (stats.sexe.G > 0) {
            const $div12 = document.createElement('div')
            $div12.style.width = String(Math.round(stats.sexe.G/(stats.sexe.G + stats.sexe.F) * 100)-1)+'%'
            $div12.innerHTML = stats.sexe.G
            $div12.className = 'boy'
            td12.appendChild($div12)
        }
        if (stats.sexe.F > 0) {
            const $div13 = document.createElement('div')
            $div13.style.width = String(Math.round(stats.sexe.F/(stats.sexe.G + stats.sexe.F) * 100)-1)+'%'
            $div13.innerHTML = stats.sexe.F
            $div13.className = 'girl'
            td12.appendChild($div13)
        }
        $stats2.appendChild(tr1)
        const tr2 = document.createElement('tr')
        const td21 = document.createElement('td')
        td21.innerText = 'Niv'
        tr2.appendChild(td21)
        const td22 = document.createElement('td')
        tr2.appendChild(td22)
        if (stats.niv.A > 0) {
            const $div22 = document.createElement('div')
            $div22.style.width = String(Math.round(stats.niv.A/(stats.niv.A + stats.niv.B + stats.niv.C + stats.niv.D) * 100)-1)+'%'
            $div22.innerHTML = stats.niv.A > 0 ? stats.niv.A : ''
            $div22.className = 'green'
            td22.appendChild($div22)
        }
        if (stats.niv.B > 0) {
            const $div23 = document.createElement('div')
            $div23.style.width = String(Math.round(stats.niv.B/(stats.niv.A + stats.niv.B + stats.niv.C + stats.niv.D) * 100))+'%'
            $div23.innerHTML = stats.niv.B > 0 ? stats.niv.B : ''
            $div23.className = 'yellow'
            td22.appendChild($div23)
        }
        if (stats.niv.C > 0) {
            const $div24 = document.createElement('div')
            $div24.style.width = String(Math.round(stats.niv.C/(stats.niv.A + stats.niv.B + stats.niv.C + stats.niv.D) * 100)-1)+'%'
            $div24.innerHTML = stats.niv.C > 0 ? stats.niv.C : ''
            $div24.className = 'orange'
            td22.appendChild($div24)
        }
        if (stats.niv.D > 0) {
            const $div25 = document.createElement('div')
            $div25.style.width = String(Math.round(stats.niv.D/(stats.niv.A + stats.niv.B + stats.niv.C + stats.niv.D) * 100))+'%'
            $div25.innerHTML = stats.niv.D > 0 ? stats.niv.D : ''
            $div25.className = 'red'
            td22.appendChild($div25)
        }
        $stats2.appendChild(tr2)
        const tr3 = document.createElement('tr')
        const td31 = document.createElement('td')
        td31.innerText = 'Abs'
        tr3.appendChild(td31)
        const td32 = document.createElement('td')
        tr3.appendChild(td32)
        if (stats.abs.A > 0) {
            const $div32 = document.createElement('div')
            $div32.style.width = String(Math.round(stats.abs.A/(stats.abs.A + stats.abs.B + stats.abs.C) * 100)-1)+'%'
            $div32.innerHTML = stats.abs.A > 0 ? stats.abs.A : ''
            $div32.className = 'green'
            td32.appendChild($div32)
        }
        if (stats.abs.B > 0) {
            const $div33 = document.createElement('div')
            $div33.style.width = String(Math.round(stats.abs.B/(stats.abs.A + stats.abs.B + stats.abs.C) * 100))+'%'
            $div33.innerHTML = stats.abs.B > 0 ? stats.abs.B : ''
            $div33.className = 'yellow'
            td32.appendChild($div33)
        }
        if (stats.abs.C > 0) {
            const $div34 = document.createElement('div')
            $div34.style.width = String(Math.round(stats.abs.C/(stats.abs.A + stats.abs.B + stats.abs.C) * 100)-1)+'%'
            $div34.innerHTML = stats.abs.C > 0 ? stats.abs.C : ''
            $div34.className = 'red'
            td32.appendChild($div34)
        }
        $stats2.appendChild(tr3)
        const tr4 = document.createElement('tr')
        const td41 = document.createElement('td')
        td41.innerText = 'Cpt'
        tr4.appendChild(td41)
        const td42 = document.createElement('td')
        tr4.appendChild(td42)
        if (stats.cpt.A > 0) {
            const $div42 = document.createElement('div')
            $div42.style.width = String(Math.round(stats.cpt.A/(stats.cpt.A + stats.cpt.B + stats.cpt.C) * 100)-1)+'%'
            $div42.innerHTML = stats.cpt.A > 0 ? stats.cpt.A : ''
            $div42.className = 'green'
            td42.appendChild($div42)
        }
        if (stats.cpt.B > 0) {
            const $div43 = document.createElement('div')
            $div43.style.width = String(Math.round(stats.cpt.B/(stats.cpt.A + stats.cpt.B + stats.cpt.C) * 100))+'%'
            $div43.innerHTML = stats.cpt.B > 0 ? stats.cpt.B : ''
            $div43.className = 'yellow'
            td42.appendChild($div43)
        }
        if (stats.cpt.C > 0) {
            const $div44 = document.createElement('div')
            $div44.style.width = String(Math.round(stats.cpt.C/(stats.cpt.A + stats.cpt.B + stats.cpt.C) * 100)-1)+'%'
            $div44.innerHTML = stats.cpt.C > 0 ? stats.cpt.C : ''
            $div44.className = 'red'
            td42.appendChild($div44)
        }
        $stats2.appendChild(tr4)
        const tr5 = document.createElement('tr')
        const td51 = document.createElement('td')
        td51.innerText = 'Fra'
        tr5.appendChild(td51)
        const td52 = document.createElement('td')
        tr5.appendChild(td52)
        if (stats.nivFr.A > 0) {
            const $div52 = document.createElement('div')
            $div52.style.width = String(Math.round(stats.nivFr.A/(stats.nivFr.A + stats.nivFr.B + stats.nivFr.C + stats.nivFr.D) * 100)-1)+'%'
            $div52.innerHTML = stats.nivFr.A > 0 ? stats.nivFr.A : ''
            $div52.className = 'green'
            td52.appendChild($div52)
        }
        if (stats.nivFr.B > 0) {
            const $div53 = document.createElement('div')
            $div53.style.width = String(Math.round(stats.nivFr.B/(stats.nivFr.A + stats.nivFr.B + stats.nivFr.C + stats.nivFr.D) * 100))+'%'
            $div53.innerHTML = stats.nivFr.B > 0 ? stats.nivFr.B : ''
            $div53.className = 'yellow'
            td52.appendChild($div53)
        }
        if (stats.nivFr.C > 0) {
            const $div54 = document.createElement('div')
            $div54.style.width = String(Math.round(stats.nivFr.C/(stats.nivFr.A + stats.nivFr.B + stats.nivFr.C + stats.nivFr.D) * 100)-1)+'%'
            $div54.innerHTML = stats.nivFr.C > 0 ? stats.nivFr.C : ''
            $div54.className = 'orange'
            td52.appendChild($div54)
        }
        if (stats.nivFr.D > 0) {
            const $div55 = document.createElement('div')
            $div55.style.width = String(Math.round(stats.nivFr.D/(stats.nivFr.A + stats.nivFr.B + stats.nivFr.C + stats.nivFr.D) * 100))+'%'
            $div55.innerHTML = stats.nivFr.D > 0 ? stats.nivFr.D : ''
            $div55.className = 'red'
            td52.appendChild($div55)
        }
        $stats2.appendChild(tr5)
        const tr6 = document.createElement('tr')
        const td61 = document.createElement('td')
        td61.innerText = 'Mat'
        tr6.appendChild(td61)
        if (stats.nivMath.A > 0) {
            const $div61 = document.createElement('div')
            $div61.style.width = String(Math.round(stats.nivMath.A/(stats.nivMath.A + stats.nivMath.B + stats.nivMath.C + stats.nivMath.D) * 100)-1)+'%'
            $div61.innerHTML = stats.nivMath.A > 0 ? stats.nivMath.A : ''
            $div61.className = 'green'
            td61.appendChild($div61)
        }
        if (stats.nivMath.B > 0) {
            const $div62 = document.createElement('div')
            $div62.style.width = String(Math.round(stats.nivMath.B/(stats.nivMath.A + stats.nivMath.B + stats.nivMath.C + stats.nivMath.D) * 100))+'%'
            $div62.innerHTML = stats.nivMath.B > 0 ? stats.nivMath.B : ''
            $div62.className = 'yellow'
            td61.appendChild($div62)
        }
        if (stats.nivMath.C > 0) {
            const $div63 = document.createElement('div')
            $div63.style.width = String(Math.round(stats.nivMath.C/(stats.nivMath.A + stats.nivMath.B + stats.nivMath.C + stats.nivMath.D) * 100)-1)+'%'
            $div63.innerHTML = stats.nivMath.C > 0 ? stats.nivMath.C : ''
            $div63.className = 'orange'
            td61.appendChild($div63)
        }
        if (stats.nivMath.D > 0) {
            const $div64 = document.createElement('div')
            $div64.style.width = String(Math.round(stats.nivMath.D/(stats.nivMath.A + stats.nivMath.B + stats.nivMath.C + stats.nivMath.D) * 100))+'%'
            $div64.innerHTML = stats.nivMath.D > 0 ? stats.nivMath.D : ''
            $div64.className = 'red'
            td61.appendChild($div64)
        }
        $stats2.appendChild(tr6)
        $div.appendChild($stats2)
        const $stats3 = document.createElement('div')
        $stats3.className = 'stats3'
        $stats3.style['display'] = 'grid'
        $stats3.style['grid-template-columns'] = 'repeat(' + Object.keys(stats.classesProv).length + ', 1fr)'
        $stats3.style['grid-template-rows'] = 'repeat(2, 1fr)'
        for (const classe in stats.classesProv){
            const $td = document.createElement('div')
            $td.innerText = classe
            $stats3.appendChild($td)
        }
        for (const classe in stats.classesProv){
            const $td = document.createElement('div')
            $td.innerText = stats.classesProv[classe]
            $stats3.appendChild($td)
        }
        $div.appendChild($stats3)
    }
}
function populateStudentsToPlace() {
    if (classes.length === 0) return
    let nbStudentsToPlace = 0
    const filtreClassesValue = [document.getElementById('filtresClassesContainer').value]
    let filtresClasses = [...filtreClassesValue]
    if (filtreClassesValue[0] === 'all') {
        filtresClasses = [...filtreClasses]
    }
	const filtreNiveauValue = document.getElementById('filtreNiveaux').value
	const filtreNiveau = filtreNiveauValue === 'all' ? ['A','B','C','D'] : [filtreNiveauValue]

	const filtreComportementValue = document.getElementById('filtreComportement').value
	const filtreComportement = filtreComportementValue === 'all' ? ['A','B','C'] : [filtreComportementValue]

	const filtreAbsencesValue = document.getElementById('filtreAbsences').value
	const filtreAbsences = filtreAbsencesValue === 'all' ? ['A','B','C'] : [filtreAbsencesValue]

    const filtreNiveauFrValue = document.getElementById('filtreFrancais').value
    const filtreNiveauFr = filtreNiveauFrValue === 'all' ? ['A','B','C','D'] : [filtreNiveauFrValue]

    const filtreNiveauMathValue = document.getElementById('filtreMaths').value
    const filtreNiveauMath = filtreNiveauMathValue === 'all' ? ['A','B','C','D'] : [filtreNiveauMathValue]
	
    const filtresOptionsValue = document.getElementById('filtresOptionsContainer').value
    const $disponiblesContainer = document.getElementById('disponiblesContainer')
    $disponiblesContainer.innerHTML = ''
    for (const eleveIndex in niveau) {
        const eleve = niveau[eleveIndex]
        // check if already in repartition
        let alreadyInRepartition = false
        for (const j in classes){
            if (classes[j].students.indexOf(Number(eleve.id))>-1) {
                alreadyInRepartition = true
                break
            }
        }
        if (alreadyInRepartition) continue
        if (filtresClasses.indexOf(String(eleve.classe)) < 0) {
            continue
        }
        let hasOption = filtresOptionsValue === 'all' || eleve[filtresOptionsValue]
        if (!hasOption) {
            continue
        }
        if (filtreAbsences.indexOf(eleve.abs)<0 && eleve.abs !== undefined) continue
        if (filtreComportement.indexOf(eleve.cpt)<0 && eleve.cpt !== undefined) continue
        if (filtreNiveau.indexOf(eleve.niv)<0 && eleve.niv !== undefined) continue
        if (filtreNiveauFr.indexOf(eleve.nivFr)<0 && eleve.nivFr !== undefined) continue
        if (filtreNiveauMath.indexOf(eleve.nivMath)<0 && eleve.nivMath !== undefined) continue
        // create eleve
        const $eleve = document.createElement('div')
        $eleve.dataset.id = eleve.id
        if (!eleve.verrou)
            $eleve.className = 'verif-eleve draggable'
        else 
            $eleve.className = 'verif-eleve locked'
        const $input = document.createElement('input')
        $input.type = 'checkbox'
        $input.value = eleve.id
        $eleve.appendChild($input)
        // sexe
        if(eleve.sexe === 'G') {
            $eleve.classList.add('boy')
        } else {
            $eleve.classList.add('girl')
        }
        checkIfLinked(eleve.id,$eleve)
        checkIfAway(eleve.id,$eleve)
        const $nom = document.createElement('div')
        $nom.innerText = eleve.nom + ' ' + eleve.prenom
        $nom.className = 'eleve-nom'
        $eleve.appendChild($nom)
        const $classe = document.createElement('div')
        $classe.innerText = eleve.classe
        $classe.className = 'eleve-classe'
        $eleve.appendChild($classe)
        // niveaux
        const $niveaux = document.createElement('div')
        $niveaux.className = 'eleve-niveaux'
        const $niv = document.createElement('div')
        $niv.className = eleve.niv
        $niv.innerHTML = ' '
        $niveaux.appendChild($niv)
        // abs
        const $abs = document.createElement('div')
        $abs.className = eleve.abs
        $abs.innerHTML = ' '
        $niveaux.appendChild($abs)
        // cpt
        const $cpt = document.createElement('div')
        $cpt.className = eleve.cpt
        $cpt.innerHTML = ' '
        $niveaux.appendChild($cpt)
        $eleve.appendChild($niveaux)
        $disponiblesContainer.appendChild($eleve)
        nbStudentsToPlace++
    }
    const nbOfStudents = Object.keys(niveau).length
    const nbOfStudentsPerClass = Math.round(nbOfStudents / config.nbClasses)
    document.getElementById('nbStudentsToPlace').innerHTML = nbStudentsToPlace+' / '+ nbOfStudents + ' - Objectif :' + nbOfStudentsPerClass + ' par classe'
	let position = {x:0,y:0}
	interact('.draggable').draggable({
        listeners: {
            start (event) {
                //console.log(event.type, event.target)
                position = {x:0, y:0}
            },
            move (event) {
                position.x += event.dx
                position.y += event.dy
                event.target.style.transform = `translate(${position.x}px, ${position.y}px)`
            },
            end (event) {
                //console.log(event.type, event.target)
                event.target.style.transform = ''
            }
        }
    })
}
function selectMultiple() {
    document.getElementById('repartition').classList.toggle('checkBoxVisible')
    if (document.getElementById('repartition').classList.contains('checkBoxVisible')) {
        document.getElementById('multcheckopt').classList.remove('is-hidden')
    } else {
        document.getElementById('multcheckopt').classList.add('is-hidden')
        document.querySelectorAll('#repartition input[type="checkbox"]').forEach(el => el.checked = false)
    }
}
function toggleLock() {
    let action = false
    document.querySelectorAll('#repartition input[type="checkbox"]:checked').forEach(elId => {
        niveau[Number(elId.value)].verrou = !niveau[Number(elId.value)].verrou
        action = true
    })
    if (action) {
        save('niveau')
        createClassesOfRepartition()
        populateStudentsToPlace()
    }
}
function removeSpaces (str){
    if(typeof str === "string")
        return str.replace(/[\s\.]+/g, '');
}
function getOptionsList(eleve) {
    const options = []
    for (const option of config.choosenOptions) {
        if (eleve[option]) {
            options.push(option)
        }
    }
    return options.join(', ')
}
function exporter(destination) {
    let separator = ';'
    if (destination === 'clipboard') {
        separator = '\t'
    }
    let repartition = ''
    let niveauFM = ''
    for (const classe of classes) {
        if (destination === 'niveaux') {
            niveauFM = separatpr + 'niv FR' + separator + 'niv Math'
        }
        repartition += 'Classe ' + classe.name + '\t' + niveauFM +'\t\n'
        for (const student of classe.students) {
            const eleve = niveau[student]
            if(destination === 'niveaux') {
                niveauFM = separator + eleve.nivFr + separator + eleve.nivMath
            }
            repartition += eleve.nom + separator + eleve.prenom + niveauFM + separator + getOptionsList(eleve) + '\n'
        }
        repartition += '\n'
    }
    if (destination === 'clipboard') {
        navigator.clipboard.writeText(repartition)
        alert('Copie dans le presse-papier, veuillez coller dans un tableur')
    } else {
        const blob = new Blob([repartition], {type:'text/plain;charset=utf-8'});
        download(blob, 'Repartition ' + config.niveau + new Date().toISOString() + '.csv', 'text/plain')
    }
}
function sauver() {
    const json = {
        config:config,
        classes:classes,
        groups:groups,
        away:away,
        MEFS:MEFS,
        niveau:niveau,
        correspondances:correspondances
    }
    const blob = new Blob([JSON.stringify(json)], {type:'text/plain;charset=utf-8'});
    download(blob, 'sauvegarde ' + config.niveau + new Date().toISOString() + '.json', 'text/plain')
}
function recuperer() {
    alert('Fonction non encore implémentée')
}
// active les boutons de tabulation
window.onload = function() {
    document.querySelectorAll('.tabs li').forEach(function(tab) {
        tab.addEventListener('click', function() {
            document.querySelectorAll('.tabs li').forEach(function(tab) {
                tab.className = '';
            });
            tab.className = 'is-active';
            openTab(tab.id.split('-')[1]);
        });
    });
    document.querySelectorAll('#niv input').forEach(function(elt) {
        elt.addEventListener('click', function(evt) {
            config.niveau = evt.target.value
        });
    });
    document.getElementById('nbClasses').oninput = (evt) => {
        config.nbClasses = evt.target.value
        displayClasses()
    }
    document.getElementById('btn-addToOptions').onclick = () => {
        addToOptions()
    }
    const $options = document.getElementById('optionsList')
    const $optionsChoisies = document.getElementById('optionsSelected')
    for (const group in options){
        const optgroup = document.createElement('optgroup')
        optgroup.label = group
        for (const option of options[group]){
            const optionElement = document.createElement('option')
            optionElement.value = option
            optionElement.innerText = option
            optgroup.appendChild(optionElement)
        }
        $options.appendChild(optgroup)
    }
    document.getElementById('btn-addOption').onclick = () => {
        addOption($options)
    }
    document.getElementById('btn-removeOption').onclick = () => {
        $optionsChoisies.querySelectorAll('option').forEach((elt) => {
            if (elt.selected){
                elt.selected = false
                document.getElementById('optionsList').querySelector('optgroup[label="' + elt.parentNode.label + '"]').appendChild(elt)
            }
        })
        refreshOptions()
        save()
    }
    document.getElementById('textareaImport').onpaste = (evt) => {
        evt.preventDefault()
        const text = evt.clipboardData.getData('text/plain')
        document.getElementById('textareaImport').value = text
        parseFile(text)
    }
    document.querySelector('#studentNext').onclick = () => {
        save('niveau')
        // on cherche l'élève suivant dans les classes
        let trouve = false
        let breaker = false
        for (const classe in oldClasses) {
            if (breaker) break
            for (const eleve of oldClasses[classe]) {
                if (trouve) {
                    studentEdit(eleve)
                    breaker = true
                    break
                }
                if (eleve === editedStudent) {
                    // recherche du suivant dans la classe
                    trouve = true
                }
            }
        }
    }
    document.querySelector('#studentPrevious').onclick = () => {
        save('niveau')
        // on cherche l'éléve precedent dans les classes
        let trouve = false
        let prec = 0, count = 0
        for (const classe in oldClasses) {
            if (trouve) break
            for (const eleve of oldClasses[classe]) {
                count++
                if (eleve === editedStudent) {
                    // recherche du precedent dans la classe
                    trouve = true
                    if(count > 1)
                        studentEdit(prec)
                    break
                }
                prec = eleve
            }
        }
    }
    document.getElementById('studentDestroy').onclick = () => {
        // remove student from niveau
        if (confirm('Voulez-vous vraiment supprimer cet eleve ?')) {
            delete niveau[editedStudent]
            save('niveau')
            displayVerifs()
            document.getElementById('studentEditor').classList.remove('is-active')
        }
    }
    document.getElementById('filtresOptionsContainer').onchange = () => {
        createClassesOfRepartition()
        populateStudentsToPlace()
    }
    document.getElementById('filtresClassesContainer').onchange = () => {
        createClassesOfRepartition()
        populateStudentsToPlace()
    }
    document.querySelectorAll('#studentClose, .delete[aria-label="close"]').forEach((elt) => {
        elt.onclick = () => {
            save('niveau')
            displayVerifs()
            document.getElementById('studentEditor').classList.remove('is-active')
        }
    })
    interact(document.getElementById('disponiblesContainer')).dropzone({
        ondrop: function (event) {
            // remove student from classes
            if (event.relatedTarget.dataset.classe !== undefined) {
                // remove from other class
                const classeId = Number(event.relatedTarget.dataset.classe)
                const studentId = Number(event.relatedTarget.dataset.id)
                classes[classeId].students.splice(classes[classeId].students.indexOf(studentId), 1)
                niveau[studentId].newclasse = 'none'
                moveAllLinkedStudentsTo(studentId, 'none')
                classes[classeId].students.sort((a, b) => (niveau[a].nom > niveau[b].nom) ? 1 : -1)
            // tri de la classe par ordre alpha selon le nom de famille
            save('classes')
            createClassesOfRepartition()
            populateStudentsToPlace()
            }
        }
      })
      .on('dropactivate', function (event) {
        event.target.classList.add('drop-activated')
      })
    load()
    bulmaSlider.attach();
    document.getElementById('nbClasses').value = config.nbClasses
}