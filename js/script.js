/*
* (C) S. COGEZ 2019
*/
"use strict"
var vars = {
    classes :{}, // Constitutions des nouvelles classes "nomClasse":[liste ids élèves attribués]
    listeClasses:[],
    optionsClasses:{}, // options de la classe "nomClasse":[liste des options]
    niveau : "",
    modeselect:0,
    MEFS : {}, // contsruit au chargement du fichier contenant les données du collège "nomMEF":[objetsEleves]
    nivTravail:[],// liste des élèves (object) dans le niveau (extrait de MEF)
    nivs:{"A":"","B":"","C":""}, // lettres utilisées par EDT, niveau d'évaluation
    elvEdit:false, // id de l'élève en cours d'édition
    places:[],//liste des id des élèves déjà placés
    effTotal:0, // nombre d'élèves en cours d'édition
    index:-3, // id du dernier élève dans l'interface, pour donner un id non utilisé aux nouveaux élèves
    opt:{}, // emojis utilisés pour les options
    step:1, // niveau d'avancement dans le fil d'ariane,
    groups:{}, // objet de tableaux d'associations d'élèves à mettre ensemble
    aways:{}, // objet de tableaux d'élèves à ne pas mettre ensemble
    memory:[], // contient les 10 derniers enregistrements, pour revenir en arrière
    memoryIndex:0
}
const champsDeBase = {"N":"Nom","P":"Prénom","C":"Classe","D":"Date naiss","S":"Sexe","cpt":"Comportement","abs":"Absences","niv":"Niveau","M":"MEF Prev","id":"ID","O":"Options"};
const champsEleve={"N":"nom","P":"prenom","C":"classe","D":"date","S":"sexe","cpt":"cpt","niv":"niv","abs":"abs","M":"MEF"};
const optionsDeBase = {
	"Jardin":"🥕",
	"Natation":"🏊‍",
	"Triathlon":"🚴",
	"Latin":"🐺",
	"FCA":"🐺",
	"Catalan":"cat",
	"Bilangue":"bi",
	"LCE":"🇪🇺",
	"AVS":"👩🏼‍🦰",
	"PAP":"🧾",
	"PPRE":"🧷",
	"Sapeur":"🔥",
	"Chorale":"👩‍🎤",
	"Sportif":"🎈",
	"Orchestre":"🎸",
	"Refus SEGPA":"❗",
    "ULIS":'ULS'
        };
var listeOptionsDansFichier = [];//exemple : "🏊‍":"NATATION SYNCHRO"
var assosOptions={};
var correspondances={};
var importation = []; // chaine à détruire une fois l'import réalisé
var utils = {
    /*
    * Créé les classes demandées de manière interactive
    * Efface le travail en cours !
    * */
    createClasses:function(classes,restore){
        vars.step=1;
        let classesExists = false,nb,niv;
        if(classes === undefined){
            vars.listeClasses = [];
            if(!confirm("Cette opération va vider toutes les classes.\nVoulez-vous continuer ?"));
            nb = Number(prompt("Combien de classes à créer ?"));
            niv = prompt("Niveau des classes ? (ex : 6, 5, 4 ou 3)");
            if(nb<1) return;
        } else {
            nb = classes.length;
            classesExists = true;
        }
        // on réinitialise tous les élèves
        if(restore === undefined){
            vars.classes={};
            vars.optionsClasses={};
            vars.nivTravail=[];
            vars.elvEdit = false;
            vars.places = [];
            vars.opt={};
            listeOptionsDansFichier = [];//exemple : "🏊‍":"NATATION SYNCHRO"
            assosOptions={};
            correspondances={};
            importation = []; // chaine à détruire une fois l'import réalisé
        }
        $("#atrier").html("");
        $("#reste").html("");
        $("#ideal").html("");
        // on vide les éléments existants
        $("#classes").html("");
        for(let i=1;i<nb+1;i++){
            if(!classesExists){
                vars.listeClasses.push(niv+i);
            }
            let classeName = (classesExists)?classes[i-1]:niv+i;
            $("#classes").append("<div id='classe"+i+"'></div>");
            $("#classe"+i).append('<div id="n'+i+'"><span class="classname">'+classeName+'</span> <button onclick="utils.placerMultiple(\'c'+i+'\');" title="Attribuer à cette classe" class="cache select-classe">👇</button><button onclick="utils.selectAll(\'c'+i+'\','+classeName+')" class="cache select-classe">✔</button><span class="classoptions"></span></div>');
            $("#classe"+i).append('<div id="c'+i+'" class="newclasse sortable"></div>');
            $("#classe"+i).append('<div id="s'+i+'" class="stats"><div class="total">0</div><table><tr><td>F/G</td><td></td></tr><tr><td>Cpt</td><td></td></tr><tr><td>Niv</td><td></td></tr><tr><td>Abs</td><td></td></tr></table><div class="prov"></div><div class="options"></div><div class="annees"></div></div>')
        };
        document.getElementsByTagName("body")[0].style.setProperty("min-width",nb*(166+12)+"px")
        if(classesExists)
            utils.makeSortable();
        utils.save()
        utils.nextStep();
    },
    Memorize(){
        let datas={
            sauvegarde:JSON.parse(JSON.stringify(vars.nivTravail)),
            classes:JSON.parse(JSON.stringify(vars.classes)),
            step:vars.step,
            listeClasses:vars.listeClasses,
            options:vars.opt,
            groups:JSON.parse(JSON.stringify(vars.groups)),
            aways:JSON.parse(JSON.stringify(vars.aways))
        };
        // si l'opération a été effectuée après une annulation, on efface tout l'historique qui suit
        if(vars.memoryIndex < vars.memory.length-1){
            while(vars.memory.length-1>vars.memoryIndex){
                vars.memory.pop();
            }
        }
        vars.memory.push(datas);
        // on supprimer la première entrée du tableau si plus de 10 données mémorisées
        if(vars.memory.length>10)
            vars.memory.shift();
        vars.memoryIndex = vars.memory.length-1;
        utils.refreshHistoryButtons();
        },
    undo(){
        vars.memoryIndex--;
        if(vars.memoryIndex > -1){
            // restore
            let data = vars.memory[vars.memoryIndex];
            vars.nivTravail = JSON.parse(JSON.stringify(data.sauvegarde));
            vars.classes = JSON.parse(JSON.stringify(data.classes));
            vars.step = data.step;
            vars.listeClasses = data.listeClasses;
            vars.opt = data.options;
            vars.groups = JSON.parse(JSON.stringify(data.groups));
            vars.aways = JSON.parse(JSON.stringify(data.aways));
            utils.restore(true);
        } else {
            vars.memoryIndex=-1;
            return false;
        }
        utils.refreshHistoryButtons();
    },
    redo(){
        vars.memoryIndex++;
        if(vars.memoryIndex < vars.memory.length){
            // restore
            let data = vars.memory[vars.memoryIndex];
            vars.nivTravail = JSON.parse(JSON.stringify(data.sauvegarde));
            vars.classes = JSON.parse(JSON.stringify(data.classes));
            vars.step = data.step;
            vars.listeClasses = data.listeClasses;
            vars.opt = data.options;
            vars.groups = JSON.parse(JSON.stringify(data.groups));
            vars.aways = JSON.parse(JSON.stringify(data.aways));
            utils.restore(true);
        } else {
            vars.memoryIndex = vars.memory.length-1;
            return false;
        }
        utils.refreshHistoryButtons();
    },
    refreshHistoryButtons(){
        let btnundoimg = document.querySelector("#undobtn img");
        let btnundo = document.getElementById("undobtn");
        let btnredoimg = document.querySelector("#redobtn img");
        let btnredo = document.getElementById("redobtn");
        if(vars.memoryIndex > 0){
            btnundoimg.src = "img/undo_icon.svg";
            btnundo.disable = false;
        } else {
            btnundoimg.src = "img/undo_nb_icon.svg";
            btnundo.disable = true;
        }
        if(vars.memoryIndex < vars.memory.length - 1) {
            btnredoimg.src = "img/redo_icon.svg";
            btnredo.disable = false;
        } else {
            btnredoimg.src = "img/redo_nb_icon.svg";
            btnredo.disable = true;

        }        
    },
    openAddOption:function(){
        let select = document.getElementById("selectOption");
        let select2 = document.getElementById("removeOption");
        select.innerHTML = "";
        select2.innerHTML = "";
        for(let i in optionsDeBase){
            let option = document.createElement("OPTION");
            option.innerText = i + "-" + optionsDeBase[i];
            option.value=i;
            if(vars.opt[optionsDeBase[i]] === undefined || vars.opt[optionsDeBase[i]]!==i){
                select.appendChild(option);
            } else {
                select2.appendChild(option);
            }
        }
        document.getElementById("addoption").className = "";
    },
    setOption(){
        let select = document.getElementById("selectOption");
        utils.addOption([optionsDeBase[select.value],select.value]);
        utils.save();
        utils.openAddOption()
    },
    addOption:function(couple){
        if(couple[0]===undefined) return;
        // couple[0] = emoji, couple[1] = texte
        vars.opt[couple[0]] = couple[1];
        utils.refreshOptions();
    },
    removeOption:function(emoji){
        delete vars.opt[optionsDeBase[emoji]];
        utils.refreshOptions();
        utils.openAddOption()
    },
    // 12/09/2005 => 2005-09-12
    toDateHTML:function(date){
        if(date.indexOf("/")<0) return "?";
        let elts = date.split("/");
        return elts[2]+"-"+elts[1]+"-"+elts[0];
    },
    // 2005-09-12 => 12/09/2005
    toDateFr:function(date){
        if(date.indexOf("-")<0) return "?";
        let elts = date.split("-");
        return elts[2]+"/"+elts[1]+"/"+elts[0];
    },
    refreshOptions:function(){
        // rafraichissement de la liste des options et des options dans l'édition
        let destination = document.getElementById("listeFiltres");
        let destination2 = document.getElementById("choixOptions");
        destination.innerHTML = "";
        destination2.innerHTML = "";
        for(let i in vars.opt){
            let span = document.createElement("SPAN");
            span.id=i;
            span.onclick = function(){utils.filter(this.id)};
            span.innerText = i+" : "+vars.opt[i].substring(0,5)+" | ";
            destination.appendChild(span);
            let input = document.createElement("input");
            input.type="checkbox";
            input.id=utils.removeSpaces(vars.opt[i]);
            input.value = i;
            input.title = vars.opt[i];
            let label = document.createElement("label");
            label.for = utils.removeSpaces(vars.opt[i]);
            label.innerText = i;
            label.title = vars.opt[i];
            destination2.appendChild(input);
            destination2.appendChild(label);
        }
    },
    removeSpaces:function(str){
        if(typeof str === "string")
            return str.replace(/[\s\.]+/g, '');
    },
    /*
    * Charge un fichier contenant les élèves extrait d'EDT
    */
    charger:function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            utils.parseFile(reader.result);
            utils.nextStep();
        };
        reader.readAsText(input.files[0]);
    },
    /**
     * Ouvre le fichier élève depuis le serveur
     */
    openFile:function(){
        let reader = new XMLHttpRequest();
        reader.onload = function(){
            utils.parseFile(reader.responseText);
            utils.nextStep();
        }
        reader.onerror = err=>{reject(err)};
        reader.open("get", "Eleves 2023.csv");
        reader.send();
    },
    /*
    * change l'étape du fil d'ariane pour revenir en arrière
    */
    prevStep:function(){
        if(vars.step > 1){
            vars.step--;
            utils.setStep();
        }
    },
    /*
    *  change l'étape du fil d'ariane pour aller à l'étape suivante
    */
    nextStep:function(){
        if(vars.step < 15){
            vars.step++;
            utils.setStep();
        }
    },
    // affiche l'étape du fil d'ariane (indications permettant d'avancer dans le travail sans rien oublier)
    setStep(){
        for(let i=1;i<15;i++){
            $("#etp"+i).attr("class","");
        }
        $("#etp"+vars.step).addClass("active");
    },
    removeQuotes(string){
        return string.replace(/^"(.+(?="$))"$/, '$1');
    },
    // lit un fichier texte contenant les élèves. Normalement exporté de EDT
    // permet d'associer les champs avec ceux attendus
    parseFile: function(result){
        vars.classes = {};
        vars.niveau = "";
        vars.groups={};
        vars.aways={};
        vars.opt={};
        $('#niveau').val("0");
        let lignes = result.split(/[\r\n]+/g);
        importation = lignes;
        // on affiche la première ligne, avec une case pour la cocher si ce sont les noms de champs
        // on affiche la 2e ligne (non vide)
        // on afffiche dessous les choix de champs possibles
        let assocs = document.getElementById("leschamps");
        assocs.innerHTML = "";
        document.getElementById("associationsChamps").className = "";
        let premiere = lignes[0].split("\t").map(this.removeQuotes);
        let deuxieme = lignes[1].split("\t").map(this.removeQuotes);
        let div0 = document.createElement("DIV");
        div0.innerText = "Première ligne du fichier";
        assocs.appendChild(div0);
        let table = document.createElement("TABLE");
        let tr1 = document.createElement("TR");
        let tr2 = document.createElement("TR");
        let tr3 = document.createElement("TR");
        for (let i = 0, len = premiere.length; i < len; i++) {
            let td = document.createElement("TD");
            td.innerText = premiere[i];
            tr1.appendChild(td);
            let tdchamp = document.createElement("TD");
            let td2 = document.createElement("TD");
            td2.innerText = deuxieme[i];
            tr3.appendChild(td2);
            let select = document.createElement("SELECT");
            let option=document.createElement("OPTION");
            option.value=0;
            option.disabled=true;
            option.selected = true;
            option.innerText = "Sélectionner";
            select.appendChild(option);
            for(let j in champsDeBase){
                let option = document.createElement("OPTION");
                option.value=j;
                option.innerText=champsDeBase[j];
                select.appendChild(option);
                if(champsDeBase[j]==premiere[i]){
                    select.style.backgroundColor = "greenyellow";
                    option.selected = true;
                    correspondances[j]=i;
                }
            }
            select.id = i; // numero du champ
            select.onchange = function(){
                let corresExist = false;
                if(correspondances["O"]!==undefined && this.value === "O"){
                    corresExist = true;
                }
                if(select.selectedIndex>0){
                    select.style.backgroundColor = "greenyellow";
                } else {
                    select.style.backgroundColor = "";
                }
                if(this.value === "O"){
                    for(let k=1,len=importation.length;k<len;k++){
                        if(importation[k]==="")continue;
                        let values = importation[k].split("\t").map(utils.removeQuotes);
                        let options = values[this.id].split(",");
                        if(corresExist){
                            // on ajoute les options dans le 1er champ désigné
                            values[correspondances["O"]] = values[correspondances["O"]]+","+values[this.id];
                            importation[k]=values.join(",");
                        }
                        for(let m=0,len=options.length;m<len;m++){
                            if(listeOptionsDansFichier.indexOf(options[m].trim())<0 && options[m].trim()!==""){
                                listeOptionsDansFichier.push(options[m].trim());
                            }
                        }
                    }
                    // affichage des associations d'options
                    let div=document.getElementById("lesoptions");
                    div.innerHTML = "";
                    for(let n=0,len=listeOptionsDansFichier.length;n<len;n++){
                        let sousdiv = document.createElement("DIV");
                        sousdiv.innerHTML = listeOptionsDansFichier[n];
                        let select2 = document.createElement("SELECT");
                        select2.dataset.nom=listeOptionsDansFichier[n];
                        let option = document.createElement("OPTION");
                        option.innerText = "Selectionner";
                        option.value=0;
                        select2.appendChild(option);
                        for(let m in optionsDeBase){
                            let option = document.createElement("OPTION");
                            option.innerText = optionsDeBase[m]+" "+m;
                            option.value=m;
                            select2.appendChild(option);
                        }
                        select2.onchange = function(){
                            assosOptions[this.value] = utils.removeSpaces(this.dataset.nom);
                            if(this.value!==0)
                                utils.addOption([optionsDeBase[this.value],this.value]);
                        }
                        sousdiv.appendChild(select2);
                        div.appendChild(sousdiv);
                    }
                }
                // on ajoute la liste des options et la possibilité de l'associer avec les codes donnés.
                if(!corresExist)
                    correspondances[this.value] = Number(this.id);
            }
            tdchamp.appendChild(select);
            tr2.appendChild(tdchamp);
        }
        table.appendChild(tr1);
        table.appendChild(tr2);
        table.appendChild(tr3);
        assocs.appendChild(table);
    },
    importPhase2:function(){
	// vérification que le MEF a été indiqué.
		if(correspondances["M"]=== undefined){
			alert("Veuillez indiquer le champ du MEF prévisionnel !");
			return;
		}
        // vérification qu'il n'y a pas 2 champs qui donnent la même chose
        let trace=[];
        for(const i in correspondances){
            if(trace.indexOf(correspondances[i])>-1){
                alert("Deux champs ont la même correspondance !\nVeuillez corriger.");
                return;
            }
            trace.push(correspondances[i]);
        }
        document.getElementById("associationsChamps").className = "cache";
        let lignes = importation;
        if(document.getElementById("firstLine").checked){
            lignes.shift();
        }
        let nbl = lignes.length;
        for(var i=0;i<nbl;i++){
            let index;
            var champs = lignes[i].split("\t").map(this.removeQuotes);
            if(correspondances["id"] === undefined){// le premier champ est un texte
                index = i;
            } else index = champs[correspondances["id"]];
            if(champs[correspondances["classe"]]=="" || champs.length<2)continue; // si pas de classe on saute (il a quitté l'étab)
            var el = {
                verrou:false, // mettre à true si bloqué dans sa classe
                newclasse:0
            };
            for(let j in correspondances){
                if(j!=="O") el[champsEleve[j]] = champs[correspondances[j]];
            }
			// traitement des options
            if(correspondances["O"] !== undefined){
                for(let i in vars.opt){
                    if(utils.removeSpaces(champs[correspondances["O"]]).indexOf(assosOptions[vars.opt[i]])>-1){
                        el[i] = true;
                    } else {
                        el[i] = false;
                    }
                }
            }
            if(vars.MEFS[champs[correspondances["M"]].substring(0,4)] === undefined){// MEFS à option supprimés
                vars.MEFS[champs[correspondances["M"]].substring(0,4)] = {};
            }
            vars.MEFS[champs[correspondances["M"]].substring(0,4)][index] = el;
        }
    },
    setNiveau:function(niv){
        vars.niveau = niv[0];
        vars.nivTravail = vars.MEFS[niv];
        var classes = [],effectif=0;
        $('#atrier').html("");
        for(let i in vars.nivTravail){
            if(vars.places.indexOf(i)>-1)continue;
            utils.appendEleve(i, vars.nivTravail[i]);
            if(classes.indexOf(vars.nivTravail[i].classe)<0){
                if(vars.nivTravail[i].classe === "") {console.log(i);continue}
                else if(vars.nivTravail[i].classe === ", ") {console.log(i);continue}
            classes.push(vars.nivTravail[i].classe);
            }
            effectif++;
			if(Number(i)>vars.index)vars.index = Number(i);
        }
        utils.triAlpha();
        classes.sort();
        vars.effTotal = effectif;
        $('#reste').html(effectif+"/"+vars.effTotal);
        utils.displayIdeal();
        $("#triclasse").children('option:not(:first)').remove();
        $.each(classes,function(i,item){
            $('#triclasse').append($('<option>',{value:item,text:item}));
        });
        utils.makeSortable();
        utils.nextStep();
        utils.save();
    },
    displayIdeal:function(){
        let nbClasses = vars.listeClasses.length
        let nbParClasse = Math.round(vars.effTotal/nbClasses);
        vars.nbParClasse = nbParClasse
        let nbF=0,nbG=0;
        for (let i in vars.nivTravail){
            if(vars.nivTravail[i].sexe === "F")nbF++;
            else if(vars.nivTravail[i].sexe === "G")nbG++;
        }
        let nbFparClasse = Math.round(nbF/nbClasses);
        let nbGparClasse = Math.round(nbG/nbClasses);
        $("#ideal").html(" Idéal : "+nbParClasse+"/classe ("+nbFparClasse+"👧/"+nbGparClasse+"🧒)");
    },
    /*
    * Récupère la position de l'élément laché dans une zone
    */
    getPos:function(evt,ui){
        // id de l'élément déplacé (id)y
        var elmId = ui.item[0].id.substring(2);
        // élément de destination
        var parent = ui.item[0].parentNode.id;
        let associes = utils.listAssocies(elmId);
        if(parent === "atrier"){
            // élève replacé dans la liste à trier
            // on enlève son ID des éléments placés.
            vars.places.splice($.inArray(elmId, vars.places),1);
            vars.nivTravail[elmId].newclasse = 0
            if(associes.length){
                for(const i of associes){
                    vars.places.splice($.inArray(i, vars.places),1);
                }
            }
        } else {
            // ajout l'id de l'élève dans la liste des élèves placés.
            if(vars.places.indexOf(elmId)<0)// si vient d'une autre classe, il est déjà placé
                vars.places.push(elmId);
            if(associes.length){
                for (const i of associes) {
                    if(vars.places.indexOf(""+i)<0)
                        vars.places.push(""+i);
                }
            }
            // alerte sur les rapprochés
            // Nom de la classe
            let classeName = $("#n"+parent.substring(1)+" .classname")[0].innerText;
            if(utils.checkAway(elmId,classeName)){
                alert("Attention, rapprochement à éviter !");
            }
        }
        // deplacement des associés
        for(const i of associes){
            // si existe quelque part dans le DOM
            let eleveDOMElement = $('#id'+i)
            if(parent === "atrier"){
                vars.nivTravail[i].newclasse = 0
            }
            if(eleveDOMElement.length !== 0) {
                eleveDOMElement.detach().appendTo('#'+parent);
            } else {
                // sinon on le recrée, car il n'était pas visible
                utils.appendEleve(i,vars.nivTravail[i],'#'+parent)
                if(vars.places.indexOf(""+i)<0)// si vient d'une autre classe, il est déjà placé
                    vars.places.push(""+i);
            }
        }
        // comptage des utilisateurs placés
        $('#reste').html((vars.effTotal - vars.places.length)+"/"+vars.effTotal);
        utils.majStats();
        utils.Memorize();
        utils.triAlpha(parent);
    },
    triAlpha:function(idClasse){
        var mylist;
        if(idClasse!== undefined)
            mylist = $("#"+idClasse);
        else
            mylist = $("#atrier");
        var listitems = mylist.children('div').get();
        listitems.sort(function(a, b) {
            return $(a).text().toUpperCase().localeCompare($(b).text().toUpperCase());
        });
        $.each(listitems, function(index, item) {
            mylist.append(item); 
        });
    },
    /**
    * Met à jour les données statistiques des classes
    * Important : recrée mes listes des classes.
    */
    majStats:function(){
        vars.classes={};let classe,prov;
        for(let i=1, len=vars.listeClasses.length;i<=len;i++){
            prov = {};
            var eleves = $("#c"+i+" > .movable").length;
            $("#s"+i+" .total").html(eleves);
            var divA,divB,divC;
            // nb filles/garçons
            var filles = $("#c"+i+" .fille").length;
            var gars = eleves-filles;
            divA = (filles)?"<div class='fille' style='width:"+Math.floor(filles/eleves*100)+"%'>"+filles+"</div>":"";
            divB = (gars)?"<div class='gars' style='width:"+Math.floor(gars/eleves*100)+"%'>"+gars+"</div>":"";
            $("#s"+i+" table tr:nth-child(1) td:last-child").html(divA+divB);
            // cpt
            var cptA = $("#c"+i+" .niveaux div:first-child.A").length;//:not(.ui-sortable-placeholder)
            var cptB = $("#c"+i+" .niveaux div:first-child.B").length;
            var cptC = $("#c"+i+" .niveaux div:first-child.C").length;
            var cptT = cptA+cptB+cptC;
            divA = (cptA)?"<div class='A' style='width:"+Math.floor(cptA/cptT*100)+"%'>"+cptA+"</div>":"";
            divB = (cptB)?"<div class='B' style='width:"+Math.floor(cptB/cptT*100)+"%'>"+cptB+"</div>":"";
            divC = (cptC)?"<div class='C' style='width:"+Math.floor(cptC/cptT*100)+"%'>"+cptC+"</div>":"";
            $("#s"+i+" table tr:nth-child(2) td:last-child").html(divA+divB+divC);
            // niveau
            var nivA = $("#c"+i+" .niveaux div:nth-child(2).A").length;
            var nivB = $("#c"+i+" .niveaux div:nth-child(2).B").length;
            var nivC = $("#c"+i+" .niveaux div:nth-child(2).C").length;
            var nivT = nivA+nivB+nivC;
            divA = (nivA)?"<div class='A' style='width:"+Math.floor(nivA/nivT*100)+"%'>"+nivA+"</div>":"";
            divB = (nivB)?"<div class='B' style='width:"+Math.floor(nivB/nivT*100)+"%'>"+nivB+"</div>":"";
            divC = (nivC)?"<div class='C' style='width:"+Math.floor(nivC/nivT*100)+"%'>"+nivC+"</div>":"";
            $("#s"+i+" table tr:nth-child(3) td:last-child").html(divA+divB+divC);
            // absences
            var absA = $("#c"+i+" .niveaux div:last-child.A").length;
            var absB = $("#c"+i+" .niveaux div:last-child.B").length;
            var absC = $("#c"+i+" .niveaux div:last-child.C").length;
            var absT = absA+absB+absC;
            divA = (absA)?"<div class='A' style='width:"+Math.floor(absA/absT*100)+"%'>"+absA+"</div>":"";
            divB = (absB)?"<div class='B' style='width:"+Math.floor(absB/absT*100)+"%'>"+absB+"</div>":"";
            divC = (absC)?"<div class='C' style='width:"+Math.floor(absC/absT*100)+"%'>"+absC+"</div>":"";
            $("#s"+i+" table tr:nth-child(4) td:last-child").html(divA+divB+divC);
             //mise à jour des classes
             classe = vars.listeClasses[i-1];
             let annees = {};
             $("#c"+i+" > div").each(
                 function(){
                     let id = $(this).attr("id").substring(2);
                     vars.nivTravail[id].newclasse = classe;
                     if(vars.classes[classe] === undefined){
                         vars.classes[classe] = [];
                     }
                     vars.classes[classe].push(id);
                     // l'année peut être inconnue
                     let annee = "????";
                     if(vars.nivTravail[id].date)
                        annee = vars.nivTravail[id].date.substring(6);
                     if(annees[annee]===undefined){
                        annees[annee] = 1;
                     } else {
                         annees[annee]++;
                     }
                }
             )
             $('#c'+i+" > div .classe").each(function(){
                if(prov[$(this).html()]=== undefined)
                    prov[$(this).html()] = {G:0,F:0};
                prov[$(this).html()][vars.nivTravail[$(this).parent()[0].id.substring(2)].sexe]++;
             });
             var ctnt="";
             for(var k in prov){
                 ctnt += "<b>"+k+"</b>/"+(prov[k].F+prov[k].G)+"|"+prov[k].F+"👧-"+prov[k].G+"🧒<br>";
             }
             $('#s'+i+" div.prov").html(ctnt);
             // comptage des options
             let options = "";
             for(let j in vars.opt){
                let poteaux = $("#c"+i).text().split(j);
                if(poteaux.length>1){
                    options += "<span title='"+vars.opt[j]+"'>"+j+"</span>:"+(poteaux.length-1)+"<br>";
                }
             }
             $("#s"+i+" div.options").html(options);
             $("#s"+i+" .annees").html("");
             for(let k in annees){
                 let ak = k;
                 if(k==="")ak="????";
                 $("#s"+i+" .annees").append("<div class='left'>"+ak+" : "+annees[k]+"</div>");
             }
        }
        utils.save();
    },
    getClassFilter(){
        return document.getElementById("triclasse").value
    },
    getNivFilter(){
        return document.getElementById("triniveau").value
    },
    getCptFilter(){
        return document.getElementById('tricomportement').value
    },
    getAbsFilter(){
        return document.getElementById('triabsences').value
    },
    /*
    * N'affiche que les élèves de la classe sélectionnée dans le dropdown
    */
    filterCaracteristics(){
        $("#atrier").html("");        
        let classe = this.getClassFilter()
        let niv = this.getNivFilter()
        let cpt = this.getCptFilter()
        let abs = this.getAbsFilter()
        for(let eleveId in vars.nivTravail){
            let eleve = vars.nivTravail[eleveId]
            if((eleve.classe === classe || classe === "all") && vars.places.indexOf(eleveId) < 0 && (eleve.niv === niv || niv === "all") && (eleve.cpt === cpt || cpt === "all") && (eleve.abs === abs || abs === "all"))
                utils.appendEleve(eleveId, eleve)
        }
        utils.triAlpha()
        utils.makeSortable()
    },
    filterReset(){
        document.getElementById("triclasse").selectedIndex = 0
        document.getElementById("triniveau").selectedIndex = 0
        document.getElementById("tricomportement").selectedIndex = 0
        document.getElementById("triabsences").selectedIndex = 0
        this.filterCaracteristics()
    },
    // n'affiche que les élèves d'une option dans les éléments à trier
    filter:function(option){
        $("#atrier").html("");
        let classe = $("#triclasse").val();
        if(option !== undefined){
            let count = 0;
            for(var i in vars.nivTravail){
                if(vars.nivTravail[i][option] && vars.places.indexOf(i) < 0 && (classe==="all" || classe === vars.nivTravail[i].classe)){
                    utils.appendEleve(i,vars.nivTravail[i]);
                    count++;
                }
            }
            utils.triAlpha();
            if(count === 0){
                $("#atrier").html("<h1>Pas/plus d'élève à placer correspondant au filtre "+option+"</h1>");
            }
        } else {
            // rien à trier
            utils.filterCaracteristics();
        }
        utils.makeSortable();
    },
    /*
    * Cherche un élève et l'affiche
    */
    chercher:function(nomEleve){
        if(nomEleve === ""){
            utils.filterCaracteristics();
            return true;
        }
        if(nomEleve.length < 3) return false;
        else {
            $("#atrier").html("");
            for(let i in vars.nivTravail){
                if(vars.nivTravail[i].nom.toUpperCase().indexOf(nomEleve.toUpperCase())>-1 || vars.nivTravail[i].prenom.toUpperCase().indexOf(nomEleve.toUpperCase())>-1){
                    if(vars.places.indexOf(i) <0){ // l'élève n'est pas encore placé
                        utils.appendEleve(i,vars.nivTravail[i]);
                    } else { // l'élève est déjà placé !
                    // on le fait bouger :D
                        $("#id"+i).effect("shake", 2000);
                    }
                }
            }
            utils.triAlpha();
            utils.makeSortable();
        }
    },
    // récupère le plus gros id d'un objet dont les
    getBiggestId:function(objet){
        let top=0;
        for(let i in objet){
            if (Number(i)>top)top=Number(i);
        }
        return top;
    },
    group:function(){
        // trouve les id des eleves sélectionnés.
        let elvs = $(".selection input:checked");
        let len = elvs.length;
        if(len===0)return false;
        let gp =[];
        let id = utils.getBiggestId(vars.groups)+1;
        for(let i=0;i<len;i++){
            let idel = Number(elvs[i].value);
            $("#id"+idel).addClass("linked");
            gp.push(idel);
            if(vars.nivTravail[idel].gp === undefined || vars.nivTravail[idel].gp==="")
                vars.nivTravail[idel].gp=""+id;
            else vars.nivTravail[idel].gp += ","+id;
        }
        vars.groups[id] = gp;
        utils.save();
        utils.Memorize();
        utils.uncheck();
    },
    ungroup:function(id){// id du groupe à supprimer
        for(let i=0,len=vars.groups[id].length;i<len;i++){
            // On fait le tour des élèves du groupe
            let liste = vars.nivTravail[vars.groups[id][i]].gp.split(",");
            // suppression de l'id du groupe d'appartenance
            let toremove = -1;
            for (let j = 0; j < liste.length; j++) {
                if(liste[j]===id){
                    toremove = j;
                    break;
                }
            }
            // on supprime l'entrée
            liste.splice(toremove,1);
            // on met à jour la liste
            vars.nivTravail[vars.groups[id][i]].gp = liste.join(",");
            // on enlève la classe uniquement s'il n'est plus lié.
            if(!vars.nivTravail[vars.groups[id][i]].gp.length)
                $("#id"+vars.groups[id][i]).removeClass("linked");
        }
        delete vars.groups[id];
        utils.save();
        utils.Memorize();
    },
    away:function(){
        // trouve les id des eleves sélectionnés.
        let elvs = $(".selection input:checked");
        let len = elvs.length;
        if(len===0)return false;
        let gp =[];
        let id = utils.getBiggestId(vars.aways)+1;
        for(let i=0;i<len;i++){
            let idel = Number(elvs[i].value);
            $("#id"+idel).addClass("away");
            gp.push(idel);
            if(vars.nivTravail[idel].ugp === undefined || vars.nivTravail[idel].ugp ==="")
                vars.nivTravail[idel].ugp=""+id;
            else vars.nivTravail[idel].ugp += id+",";
        }
        vars.aways[id] = gp;
        utils.save();
        utils.Memorize();
        utils.uncheck();
    },
    unaway:function(id){// id du groupe à supprimer
        for(let i=0,len=vars.aways[id].length;i<len;i++){
            // On fait le tour des élèves du groupe
            let liste = vars.nivTravail[vars.aways[id][i]].ugp.split(",");
            $("#id"+vars.aways[id][i]).removeClass("away");
            let toremove = -1;
            for (let j = 0; j < liste.length; j++) {
                if(liste[j]===id)
                    toremove = j;
            }
            // on supprime l'entrée
            liste.splice(toremove,1);
            // on met à jour la liste
            vars.nivTravail[vars.aways[id][i]].ugp = liste.join(",");
        }
        delete vars.aways[id];
        utils.Memorize();
        utils.save();
    },
    /*
    * vérifie des personnes sont liées et on les (dé)coche tous
    */
    checkLinks:function(id){
        let checked = $("#id"+id+" .selection input").is(':checked');
        if(vars.nivTravail[id].gp !== undefined && vars.nivTravail[id].gp !== ""){
            let associes = [];
            let liste = vars.nivTravail[id].gp.split(",");
            for (let i = 0; i < liste.length; i++) {
                let gp = vars.groups[liste[i]];
                for (let j = 0; j < gp.length; j++) {
                    if(associes.indexOf(gp[j])<0 && gp[j]!==id)associes.push(gp[j]);
                    if(checked){
                        $("#id"+gp[j]+" .selection input").prop("checked", true);
                    } else {
                        $("#id"+gp[j]+" .selection input").prop("checked", false);
                    }
                }
            }
            return associes;
        } else return [];
    },
    listAssocies:function(id){
        if(vars.nivTravail[id].gp !== undefined && vars.nivTravail[id].gp !== ""){
            let associes = [];
            let liste = vars.nivTravail[id].gp.split(",");
            for (const i of liste) {
                let gp = vars.groups[i];
                for (const j of gp) {
                    if(associes.indexOf(j)<0 && j!==id)associes.push(j);
                }
            }
            return associes;
        } else return [];
    },
    checkAway:function(id, classeName){
        let problem=false;
        if(vars.classes[classeName]===undefined)return problem;
        if(vars.nivTravail[id].ugp !== undefined && vars.nivTravail[id].ugp !==""){
            let liste = vars.nivTravail[id].ugp.split(",");
            for (let i = 0; i < liste.length; i++) {
                let gp = vars.aways[liste[i]];
                for (let j = 0; j < gp.length; j++) {
                    if(gp[j]!==id && vars.classes[classeName].indexOf(""+gp[j])>-1){
                        problem = true; // un élève à éloigner est déjà dans la classe
                    }
                }
            }
        }
        return problem;
    },
    openGroups:function(type){
        document.getElementById("groupes").className = "";
        let destination = document.getElementById("listeGroupes");
        let liste;
        if(type==="away"){
            liste=vars.aways;
            destination.innerHTML = "<h3>Liste des éloignements</h3>";
        } else {
            destination.innerHTML = "<h3>Liste des regroupements</h3>";
            liste = vars.groups;
        }
        for (let i in liste) {
            const group = liste[i];
            let ul = document.createElement("UL");
            let button = document.createElement("button");
            button.innerText="Supprimer ce groupe";
            button.dataset.idgp = i;
            if(type === "group"){
                button.onclick=function(){
                    if(confirm("Vous êtes sur le point de supprimer le groupe "+this.dataset.idgp+"\nConfirmez-vous ?")){
                        utils.ungroup(this.dataset.idgp);
                        utils.openGroups("group");
                    }
                }
            } else {
                button.onclick=function(){
                    if(confirm("Vous êtes sur le point de supprimer l'éloignement' "+this.dataset.idgp+"\nConfirmez-vous ?")){
                        utils.unaway(this.dataset.idgp);
                        utils.openGroups("away");
                    }
                }
            }
            ul.append(((type=="group")?"Groupe ":"Eloignement ")+i, button);
            for (let j = 0; j < group.length; j++) {
                const elv = group[j];
                let li=document.createElement("LI");
                li.innerText=vars.nivTravail[elv].nom.toUpperCase()+" "+vars.nivTravail[elv].prenom;
                ul.append(li);
            }
            destination.append(ul);
        }
    },
    /*
    * Génère et télécharge le fichier des classes
    */
    download:function(content,ext){
        // vérification qu'il n'y a pas d'éloignement demandé rapprochés.
        let pb = false;
        let message = "";
        for(const idClasse in vars.classes){
            const classe = vars.classes[idClasse]
            for(const id of classe){
                if(vars.nivTravail[id].ugp !== ""){ // liste des groupes d'éloignement comprenant l'élève
                    let groups = vars.nivTravail[id].ugp.split(",");
                    for(const group of groups){
                        // on regarde si un élève du groupe est dans la même classe
                        for(let l=0;l<vars.aways[group].length;l++){
                            let awayid =String(vars.aways[group][l]);
                            if(awayid===String(id))
                                continue;
                            if(classe.indexOf(awayid)>-1){
                                pb = true;
                                message = vars.nivTravail[id].nom+" et\n"+vars.nivTravail[awayid].nom+"\nsont dans la même classe !!\nOK pour continuer\nAnnuler pour reprendre.";
                                break;
                            }
                        }
                        if(pb)break;
                    }
                    if(pb)break;
                }
                if(pb)break;
            }
            if(pb)break;
        }
        if(pb){
            if(!confirm(message)){
                return;
            }
        }
        let idel,el;if(ext=== undefined)ext="csv";
        if(content === undefined){
            content="Nouvelle classe\tNOM\tPrénom\tOptions\tProvenance\r\n";
            for(let i in vars.classes){
                for(let j=0;j<vars.classes[i].length;j++){
                    idel = Number(vars.classes[i][j]);
                    el = vars.nivTravail[idel];
                    content += i+"\t"+el.nom+"\t"+el.prenom+"\t"+utils.getOptionsText(el)+"\t"+el.classe+"\r\n";
                }
            }
        }
        var contentType = "text/plain";
        var filename = "Niveau "+vars.niveau+"."+ext;
        var blob = new Blob([content], {type:contentType});
        download(blob, filename, contentType);
    },
    /*
    * ajoute un élève dans le DOM
    */
    appendEleve:function(i,elv,target){
        if(target === undefined)target = "#atrier";
        if(elv.gp===undefined)elv.gp="";
        if(elv.ugp===undefined)elv.ugp="";
        if(!elv.classe)elv.classe="";if(!elv.cpt)elv.cpt="A";if(!elv.niv)elv.niv="A";if(!elv.abs)elv.abs="A";
        $(target).append("<div id='id"+i+"' class='movable "+((elv.sexe === "F")?"fille":"gars")+((elv.verrou)?" locked":"")+((elv.gp.split(",")[0]>0)?" linked":"")
        +((elv.ugp.split(",")[0]>0)?" away":"")
        +"'>"+
        "<div class='selection"+((vars.modeselect===1)?"":" cache")+"'><input type='checkbox' value='"+i+"' onclick='utils.checkLinks("+i+")'></div>"
        +"<span class='nom' title='"+elv.prenom+"\n"+elv.date+"'>"+(elv.nom.toUpperCase())+
        " "+elv.prenom+"</span> <span class='classe' title='"+elv.classe+"'>"+elv.classe+"</span>"+
        "<div class='niveaux' title='Comportement : "+elv.cpt+"&#10;Niveau : "+elv.niv+"&#10;Absences : "+elv.abs+
        "&#10;cliquer pour changer' onclick='utils.editEleve("+i+");'><div class='"+elv.cpt+"'> </div><div class='"+elv.niv+"'> </div><div class='"+elv.abs+"'> </div></div>"+
        "<span class='option'>"+utils.getOptions(elv)+"</span></div>");
    },
    /*
    * affiche les options
    */
    getOptions:function(elt){
        let chaine = "";
        for(let i in vars.opt){
            if(elt[i]){
                chaine += "<span title='"+vars.opt[i]+"'>"+i+"</span>";
            }
        }
        return chaine;
    },
    getOptionsText:function(elt){
        let chaine = "",sep="";
        for(let i in vars.opt){
            if(chaine!=="")sep=",";
            if(elt[i]){
                chaine += sep+vars.opt[i];
            }
        }
        return chaine;
    },
    /*
    * 
    * raffraichit l'affichage de l'élève après édition
    * */
    refreshEleve:function(id){
        var elv = vars.nivTravail[id];
        $("#id"+id+" span.nom").text(elv.nom.toUpperCase()+" "+elv.prenom);
        $("#id"+id+" span.nom").attr("title",elv.prenom+(elv.date?"\n"+elv.date:""));
        $("#id"+id+" span.classe").text(elv.classe);
        $("#id"+id+" span.option").html(utils.getOptions(elv));
        let sex = "gars";
        if(elv.sexe === "F")sex = "fille";
		let verrou = "";
		if(elv.verrou) verrou = " locked";
		let gp = "";
		if(elv.gp !=="") gp = " linked";
		let away = "";
		if(elv.ugp !== "") away = " away";
        $("#id"+id).attr("class", "movable "+sex+verrou+gp+away);
        var divs = $("#id"+id+" .niveaux > div");
        divs[0].className = elv.cpt;
        divs[1].className = elv.niv;
        divs[2].className = elv.abs;
        $("#id"+id+" .niveaux").attr("title","Comportement : "+elv.cpt+"\nNiveau : "+elv.niv+"\nAbsences : "+elv.abs + "\nCliquer pour changer");
    },
    /*
    * Met le formulaire d'édition à jour en fonction des données de la base
    * */
    editEleve(id){
        let elv;
        if(id === undefined){
            elv = {nom:"",prenom:"",classe:"",sexe:"F",cpt:"A",niv:"A", abs:"A",date:""};
            // on attribue les options
            for(let i in vars.opt){
                elv[i] = false;
            }
            vars.elvEdit = false;
        } else {
            elv = vars.nivTravail[id];
            vars.elvEdit = id;
        }
        $("#dialog-niveaux").dialog("option", "title", elv.nom+" "+elv.prenom+" "+elv.classe);
        $('#name').val(elv.nom);
        $('#forname').val(elv.prenom);
        $('#classanterior').val(elv.classe);
        $('#naissance').val(utils.toDateHTML(elv.date));
        $("input[name=genre][value="+elv.sexe+"]").prop('checked', true);
        $("input[name=cpt][value="+elv.cpt+"]").prop('checked', true);
        $("input[name=niv][value="+elv.niv+"]").prop('checked', true);
        $("input[name=abs][value="+elv.abs+"]").prop('checked', true);
        // on raffraichit le cochage des options
        for(let i in vars.opt){
            if(elv[i]){
                $('#'+utils.removeSpaces(vars.opt[i])).prop("checked", true);
            } else {
                $('#'+utils.removeSpaces(vars.opt[i])).prop("checked", false);
            }
        }
        $("#dialog-editeleve").dialog("open");
    },
    // enregistre les modifications apportées à l'élève en cours d'édition
    saveEleve:function(){
        let index;
        if(vars.elvEdit!==false){
            index = vars.elvEdit;
            let elv = vars.nivTravail[index];
            elv.nom = $('#name').val();
            elv.prenom=$('#forname').val();
            elv.classe=($('#classanterior').val()!=="")?$('#classanterior').val():"ext";
            elv.date=utils.toDateFr($('#naissance').val());
            elv.sexe=$("input[name=genre]:checked").val();
            elv.cpt=$("input[name=cpt]:checked").val();
            elv.niv=$("input[name=niv]:checked").val();
            elv.abs=$("input[name=abs]:checked").val();
        } else {
            vars.index++;
            index = vars.index;
            vars.nivTravail[index]={
                nom:$('#name').val(),
                prenom:$('#forname').val(),
                classe:($('#classanterior').val()!=="")?$('#classanterior').val():"ext",
                date:utils.toDateFr($("#naissance").val()),
                sexe:$("input[name=genre]:checked").val(),
                cpt:$("input[name=cpt]:checked").val(),
                niv:$("input[name=niv]:checked").val(),
                abs:$("input[name=abs]:checked").val(),
                newclasse:0
            };
            utils.displayIdeal();
        }
        for(let i in vars.opt){
            if($("#"+utils.removeSpaces(vars.opt[i])).prop('checked'))
                vars.nivTravail[index][i] = true;
            else
                vars.nivTravail[index][i] = false;
        }
        utils.makeSortable();
        if(vars.elvEdit===false){
            utils.appendEleve(index,vars.nivTravail[index]);
            vars.effTotal++;
        } else {
            vars.elvEdit = false;
            utils.refreshEleve(index);
            }
        utils.majStats();
        utils.Memorize();
        utils.save();
    },
    //supprime l'élève
    deleteEleve(){
        if(confirm("Vous êtes sur le point de supprimer l'élève "+vars.nivTravail[vars.elvEdit].nom+"\nConfirmez-vous ?")){
            //remove from DOM
            document.getElementById("id"+vars.elvEdit).remove();
            delete vars.nivTravail[vars.elvEdit];
            for(const classe of vars.classes){
                if(classe.includes(vars.elvEdit)){
                    classe.splice(classe.indexOf(vars.elvEdit),1)
                }
            }
            vars.elvEdit = false;
            utils.save()
            utils.Memorize();
            return true;
        } else {
            return false;
        }
    },
    /*
    * Sauvegarde les données dans la mémoire de l'ordinateur
    */
    save:function(){
        localStorage.sauvegarde = JSON.stringify(vars.nivTravail);
        localStorage.classes = JSON.stringify(vars.classes);
        localStorage.step = vars.step;
        localStorage.listeClasses = JSON.stringify(vars.listeClasses);
        localStorage.options = JSON.stringify(vars.opt);
        localStorage.groups = JSON.stringify(vars.groups);
        localStorage.aways = JSON.stringify(vars.aways);
        localStorage.niveau = vars.niveau
    },
    sauvegarde:function(){
        let contenu = {"sauvegarde":vars.nivTravail,"classes":vars.classes,"step":vars.step,"listeClasses":vars.listeClasses,"options":vars.opt,"groups":vars.groups,"aways":vars.aways};
        utils.download(JSON.stringify(contenu),"txt");
    },
    recupsauvegarde(){
        // on affiche le formulaire
        document.getElementById("uploadFile").className = "";
    },
    closeUpload(){
        document.getElementById('uploadFile').className='cache';
    },
    closeAddOption(){
        document.getElementById("addoption").className="cache";
    },
    closeGroupes(){
        document.getElementById("groupes").className="cache";
    },
    upload:function(event){
        var input = event.target;
        var reader = new FileReader();
        reader.onload = function(){
            if (reader.result !== undefined){
                let data = JSON.parse(reader.result);
                vars.nivTravail = data.sauvegarde;
                vars.classes = data.classes;
                vars.step = data.step;
                vars.listeClasses = data.listeClasses;
                vars.opt = data.options;
                if(data.niveau !== undefined)
                    vars.niveau = data.niveau
                if(data.groups !== undefined)
                    vars.groups = data.groups;
                if(data.aways !== undefined)
                    vars.aways = data.aways;
                utils.restore();
            } else {
                console.log("Oups");
            }
        };
        reader.readAsText(input.files[0]);
        // on cache le formulaire
        document.getElementById("uploadFile").className = "cache";
    },
    restore:function(fromMemory=false){
        // on recrée les emplacements classes
        utils.createClasses(vars.listeClasses, true);
        if(vars.step !== undefined)
            utils.setStep();
        // on place les élèves dans les classes
        let idel;
        vars.elvEdit = false;
        vars.places = [];
        vars.index = -3;
        for (const i in vars.classes){
            //vars.niveau = String(index)[0];
            // retirer les doublons dans la classe au cas où
            const classe = [...new Set(vars.classes[i])]
            vars.classes[i] = classe
            for(let idEleve of classe){
                idel = Number(idEleve);
                vars.places.push(String(idel));
                utils.appendEleve(idel,vars.nivTravail[idel],"#c"+i.substring(i.length - 1));
            }
        }
        let classes = [],effectif=0,effectifTotal=0;
        $('#atrier').html("");
        for(let i in vars.nivTravail){
            if(vars.index<Number(i))vars.index=Number(i);
            effectifTotal++;
            if(vars.places.indexOf(i)>-1)continue;
            const newclasse = vars.nivTravail[i].newclasse
            if(newclasse !== 0){
                utils.appendEleve(i, vars.nivTravail[i], "#c"+(newclasse.substring(newclasse.length - 1)))
                if(vars.classes[newclasse] === undefined) vars.classes[newclasse]=[]
                vars.classes[newclasse].push(i)
                vars.places.push(i)
            } else {
                utils.appendEleve(i, vars.nivTravail[i]);
                effectif++;
            }
            if(classes.indexOf(vars.nivTravail[i].classe)<0){
                if(vars.nivTravail[i].classe === "") {console.log(i);continue}
                else if(vars.nivTravail[i].classe === ", ") {console.log(i);continue}
                classes.push(vars.nivTravail[i].classe);
            }
        }
        utils.triAlpha();
        classes.sort();
        vars.effTotal = effectifTotal;
        $('#reste').html(effectif+"/"+effectifTotal);
        utils.displayIdeal();
        $("#triclasse").children('option:not(:first)').remove();
        $.each(classes,function(i,item){
            $('#triclasse').append($('<option>',{value:item,text:item}));
        });
        utils.refreshOptions();
        utils.makeSortable();
        utils.majStats();
        if(!fromMemory)utils.Memorize()
    },
    /*
    * Met/enlève le mode sélection multiple d'élèves
    */
    toggleMultiple(){
        vars.modeselect = 1-vars.modeselect;
        // tout uncheck
        $(".selection input").prop('checked', false);
        $('.selection, .select-classe, #multcheckopt').toggleClass('cache');
    },
    uncheck(){
        $(".selection input").prop('checked', false);
    },
    // sélectionne / déselectionne tous les élèves affichés dans la place
    selectAll(id){
        let total = $("#"+id+" .selection input").length;
        let checked = $("#"+id+" .selection input:checked").length;
        if(total === checked){
            $("#"+id+" .selection input").prop("checked", false);
        } else {
            $("#"+id+" .selection input").prop("checked", true);
        }
    },
    /*
    * déplace les élèves sélectionnés dans la classe choisie
    */
    placerMultiple(target, classeId){
        // récupérer la sélecion
        $('.selection input:checked').each(function(){
            let id = $(this).val();
            let aeloigner = utils.checkAway(id,classeId);
            if(!vars.nivTravail[id].verrou && !aeloigner){
                // on ne déplace pas les élèves vérouilles !
                $('#id'+id).detach().appendTo('#'+target);
                if(vars.places.indexOf(id)<0) {
                    vars.places.push(id);
                } else if(target === 'atrier'){
                    vars.places.splice(vars.places.indexOf(id),1)
                }
            } else {
                let message = "";
                if(vars.nivTravail[id].verrou) message+="Elève "+vars.nivTravail[id].nom+" vérouillé\n";
                if(aeloigner) message+="Elève "+vars.nivTravail[id].nom+" à éloigner";
                alert(message);
            }
        })
        $(".selection input").prop('checked', false);
        // calcul du reste des élèves à placer.
        $('#reste').html((vars.effTotal - vars.places.length)+"/"+vars.effTotal);
        // tri alpha
        utils.triAlpha(target);
        // calculs stats
        utils.majStats();        
        // sauvegarde
        utils.save();
        utils.Memorize();
        // retrait de la sélection multiple
        utils.toggleMultiple();
    },
    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    },
    placeStudents(){
        let students = document.querySelectorAll('#atrier > div')
        let ids = [], classesIds=[], cId = 0
        for (let [id,student] of students.entries()){
            ids.push([id, student.id])
        }
        this.shuffle(ids)
        let classes = document.querySelectorAll('#classes .newclasse')
        for (const classe of classes){
            classesIds.push(classe.id)
        }
        let cIdMax = classesIds.length
        for(const std of ids){
            // on ne place les élèves que dans les classes où il reste de la place
            do {
                cId = (cId + 1)%cIdMax
                if(cId === 0) this.shuffle(classesIds)
            }
            while ($('#'+classesIds[cId]).children().length >= vars.nbParClasse)
            let id = std[1].substring(2)
            $('#'+std[1]).detach().appendTo('#'+classesIds[cId]).addClass('justmoved')
            setTimeout(()=>{$('#'+std[1]).removeClass('justmoved')},60000)
            if(vars.places.indexOf(id)<0)vars.places.push(id);            
        }
        for(const classe of classes){
            utils.triAlpha(classe.id)
        }
        // calculs stats
        utils.majStats();        
        // sauvegarde
        utils.save();
        utils.Memorize()
        $('#reste').html((vars.effTotal - vars.places.length)+"/"+vars.effTotal);
    },
    // bloque/débloque les élèves cochés dans leur classe
    toggleLock:function(){
        if(!vars.modeselect) return;
        $('.selection input:checked').each(function(){
            let id = $(this).val();
            $('#id'+id).toggleClass("locked");
            if(vars.nivTravail[id].verrou){
                vars.nivTravail[id].verrou = false;
            } else vars.nivTravail[id].verrou = true;
        });
        utils.toggleMultiple();
        utils.makeSortable();
        utils.save();
    },
    makeSortable:function(){
        $(".sortable").sortable({cancel:".locked",connectWith:".sortable",stop:utils.getPos}).disableSelection();
    }
};
$(function(){
    utils.makeSortable();
    $('#dialog-editeleve').dialog({
        autoOpen:false,
        resizable:false,
        height:360,
        width:720,
        modal:true,
        title:'Completer et valider pour ajouter un eleve',
        buttons:{
            "Eleve précédent":function(){
                var eleveid = vars.elvEdit;
                utils.saveEleve();
                // cas d'un ajout d'élève
                if(eleveid === false){
                    utils.editEleve()
                    return;
                }
                let thisElt = document.getElementById("id"+eleveid);
                if(thisElt.previousSibling!==null){
                    eleveid = thisElt.previousSibling.id.substring(2);
                    utils.editEleve(eleveid);
                } else {
                    alert("Pas d'élève trouvé");
                    $(this).dialog("close"); 
                }
            },
            "Eleve suivant":function(){
                var eleveid = vars.elvEdit;
                utils.saveEleve();
                // cas d'un ajout d'élève
                if(eleveid === false){
                    utils.editEleve()
                    return;
                }
                let thisElt = document.getElementById("id"+eleveid);
                if(thisElt.nextSibling!==null){
                    eleveid = thisElt.nextSibling.id.substring(2);
                    utils.editEleve(eleveid);
                } else {
                    alert("Pas d'élève trouvé");
                    $(this).dialog("close"); 
                }
            },
            "Valider":function(){
                utils.saveEleve();
                $(this).dialog("close");
            },
            "Annuler":function(){
                $(this).dialog("close");
            },
            "Supprimer":function(){
                if(utils.deleteEleve())
                    $(this).dialog("close");
            }
        }
    });
    $('#explis').dialog({
        autoOpen: false,
        width:650,
        show:{effect:"blind",duration:200},
        hide:{effect:"explode", duration:500}
    });
    // récup d'une sauvegarde locale
    if(localStorage.classes !== undefined){
        // récup de la liste des noms de classes créées
        vars.listeClasses = JSON.parse(localStorage.listeClasses);
        // récup des élèves à trier
        vars.nivTravail = JSON.parse(localStorage.sauvegarde);
        // récup des constitutions de classes
        vars.classes = JSON.parse(localStorage.classes);
        // récup des options définies
        vars.opt = JSON.parse(localStorage.options);
        // recup des groupements
        if(localStorage.groups !== undefined)
            vars.groups = JSON.parse(localStorage.groups);
        // recup des eloignements
        if(localStorage.aways !== undefined)
            vars.aways = JSON.parse(localStorage.aways);
        if(localStorage.niveau !== undefined)
            vars.niveau = localStorage.niveau
        // récup de l'avancement dans le déroulement des opérations
        if(localStorage.step !== undefined && localStorage.step !== "undefined"){
            vars.step = localStorage.step;
        }
        utils.restore();
    }
});